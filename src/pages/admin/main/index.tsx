import LayoutAdmin from '@/components/Layout/Admin/Admin';
import Main from '@/containers/Admin/Main/Main';

export default function AdminMain() {
  return (
    <LayoutAdmin>
      <Main />
    </LayoutAdmin>
  );
}
