import LayoutAdmin from '@/components/Layout/Admin/Admin';
import AdminUser from '@/containers/Admin/User/AdminUser';

export default function AdminUsers() {
  return (
    <LayoutAdmin>
      <AdminUser />
    </LayoutAdmin>
  );
}
