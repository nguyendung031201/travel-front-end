import LayoutAdmin from '@/components/Layout/Admin/Admin';
import AdminTour from '@/containers/Admin/AdminTour/AdminTour';

export default function AdminTours() {
  return (
    <LayoutAdmin>
      <AdminTour />
    </LayoutAdmin>
  );
}
