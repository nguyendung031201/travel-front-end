import LayoutAdmin from '@/components/Layout/Admin/Admin';
import AdminBanner from '@/containers/Admin/Banner/AdminBanner';

export default function AdminBanners() {
  return (
    <LayoutAdmin>
      <AdminBanner isCreate />
    </LayoutAdmin>
  );
}
