import LayoutAdmin from '@/components/Layout/Admin/Admin';
import AdminFeedBack from '@/containers/Admin/FeedBack/AdminFeedBack';

export default function AdminBanners() {
  return (
    <LayoutAdmin>
      <AdminFeedBack />
    </LayoutAdmin>
  );
}
