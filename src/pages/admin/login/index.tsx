/* eslint-disable no-console */
import { useRouter } from 'next/router';
import { useState } from 'react';

import servicesLogin from '@/services/login';

export interface IUser {
  username: string;
  password: string;
  role: string;
}

export default function LoginPage() {
  const [userValue, setUserValue] = useState<IUser>({
    username: '',
    password: '',
    role: '',
  });

  const router = useRouter();
  const [message, setMessage] = useState('');
  const onLogin = async () => {
    setMessage('');
    const formData = new FormData();

    formData.append('username', userValue.username);
    formData.append('password', userValue.password);

    const res = await servicesLogin.login(formData, () =>
      router.push('/admin')
    );
    // router.push('/admin');
    if (res?.data?.error) {
      setMessage(res?.data?.error?.message);
    }
  };

  return (
    <section className="h-screen">
      <div className="h-full px-6 text-gray-800 lg:px-12">
        <div className="flex h-full flex-wrap items-center justify-center lg:justify-between xl:justify-center">
          <div className="mb-12 grow-0 basis-auto md:mb-0 md:w-9/12 lg:w-6/12 xl:w-6/12">
            <img
              src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/draw2.webp"
              className="w-full"
              alt="Sample image"
            />
          </div>
          <div className="mb-12 md:mb-0 md:w-8/12 lg:w-5/12 xl:ml-20 xl:w-5/12">
            <div className="mb-6">
              <input
                type="text"
                className=" m-0 block w-full rounded border border-solid border-gray-300 bg-white bg-clip-padding px-4 py-2 text-xl font-normal text-gray-700 transition ease-in-out focus:border-blue-600 focus:bg-white focus:text-gray-700 focus:outline-none"
                placeholder="Email address"
                value={userValue.username}
                onChange={(e) =>
                  setUserValue({ ...userValue, username: e.target.value })
                }
                onKeyDown={(e) => {
                  if (e.key === 'Enter') {
                    onLogin();
                  }
                }}
              />
            </div>
            <div className="mb-6">
              <input
                type="password"
                className=" m-0 block w-full rounded border border-solid border-gray-300 bg-white bg-clip-padding px-4 py-2 text-xl font-normal text-gray-700 transition ease-in-out focus:border-blue-600 focus:bg-white focus:text-gray-700 focus:outline-none"
                placeholder="Password"
                value={userValue.password}
                onChange={(e) =>
                  setUserValue({ ...userValue, password: e.target.value })
                }
                onKeyDown={(e) => {
                  if (e.key === 'Enter') {
                    onLogin();
                  }
                }}
              />
            </div>
            <p className="mb-3 h-6 text-red-600">{message}</p>
            <div className="text-center lg:text-left">
              <button
                onClick={onLogin}
                type="button"
                className="inline-block rounded bg-blue-600 px-7 py-3 text-sm font-medium uppercase leading-snug text-white shadow-md transition duration-150 ease-in-out hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg"
              >
                Login
              </button>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
