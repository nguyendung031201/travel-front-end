import LayoutAdmin from '@/components/Layout/Admin/Admin';
import AdminBooking from '@/containers/Admin/Booking/AdminBooking';

export default function AdminBookings() {
  return (
    <LayoutAdmin>
      <AdminBooking />
    </LayoutAdmin>
  );
}
