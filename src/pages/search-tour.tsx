/* eslint-disable import/extensions */
import SearchTour from '@/containers/SearchTour';
import type { IIteamTour } from '@/services/tour';
import servicesTour from '@/services/tour';

const Index = ({ tours }: { tours: IIteamTour[] }) => {
  return (
    <>
      <SearchTour tours={tours} />
    </>
  );
};
export async function getServerSideProps() {
  // Fetch data from external API
  const resTour = await servicesTour.getAll();
  let tours: IIteamTour[] = [];

  if (resTour?.data?.tour) tours = resTour.data.tour;
  // Pass data to the page via props
  return {
    props: { tours },
  };
}

export default Index;
