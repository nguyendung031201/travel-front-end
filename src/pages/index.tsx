import HomePage from '@/containers/Home';
import type { IItemBanner } from '@/services/banner';
import type { IIteamTour } from '@/services/tour';
import servicesTour from '@/services/tour';

import servicesBanner from '../services/banner';

const Index = ({ banners, tours }: any) => {
  return (
    <>
      <HomePage banners={banners} tours={tours} />
    </>
  );
};

export async function getServerSideProps() {
  // Fetch data from external API
  const resBanner = await servicesBanner.getAll();
  const resTour = await servicesTour.getAll();

  let banners: IItemBanner[] = [];
  let tours: IIteamTour[] = [];

  if (resBanner?.data?.banner) banners = resBanner.data.banner;
  if (resTour?.data?.tour) tours = resTour.data.tour;
  // Pass data to the page via props
  return {
    props: { banners, tours },
  };
}

export default Index;
