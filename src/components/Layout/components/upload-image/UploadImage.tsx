/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable tailwindcss/no-custom-classname */
import React, { useState } from 'react';
import Cropper from 'react-cropper';
import styled from 'styled-components';

const Styles = styled.div`
  .box {
    display: inline-block;
    padding: 10px;
    box-sizing: border-box;
  }
  .img-preview {
    overflow: hidden;
  }
`;
export interface IUploadAndDisplayImage {
  setCropper: (e: any) => void;
  oldImage: string;
  aspectRatio?: number;
}

export default function UploadAndDisplayImage({
  setCropper = () => {},
  oldImage,
  aspectRatio,
}: IUploadAndDisplayImage) {
  const [image, setImage] = useState('');
  const onChange = (e: any) => {
    e.preventDefault();
    let files;
    if (e.dataTransfer) {
      files = e.dataTransfer.files;
    } else if (e.target) {
      files = e.target.files;
    }
    const reader = new FileReader();
    reader.onload = () => {
      setImage(reader.result as any);
    };
    reader.readAsDataURL(files[0]);
  };

  return (
    <Styles className="mt-3">
      <input type="file" onChange={onChange} />
      <div className="flex gap-6">
        <Cropper
          className="mt-3 w-1/2"
          zoomable={false}
          zoomTo={0}
          aspectRatio={aspectRatio}
          preview=".img-preview"
          src={image}
          viewMode={1}
          minCropBoxHeight={10}
          minCropBoxWidth={10}
          background={false}
          responsive={true}
          autoCropArea={1}
          checkOrientation={false}
          onInitialized={(instance: any) => {
            setCropper(instance);
          }}
          guides={true}
        />
        {image ? (
          <div className="mt-3 w-1/2">
            Xem trước:
            <div
              className="img-preview mt-3 border"
              style={{ width: '100%', height: '300px' }}
            />
          </div>
        ) : (
          <div className="mt-3 w-1/2">
            <img src={oldImage} alt="" />
          </div>
        )}
      </div>
    </Styles>
  );
}
