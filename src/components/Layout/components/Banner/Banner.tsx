/* eslint-disable tailwindcss/enforces-negative-arbitrary-values */
export default function Banner() {
  return (
    <div className="">
      <div className="relative w-full overflow-hidden after:clear-both after:block after:content-['']">
        <div
          className="relative float-left -mr-[100%] w-full transition-transform duration-[600ms] ease-in-out motion-reduce:transition-none"
          data-te-carousel-active
          data-te-carousel-item
          // style="backface-visibility: hidden"
        >
          <img
            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTinUzh1ps04S9dvOs8rH6S0szmJrhf5HNHon4Z91o_ywjqh66xeTpLpftygrx6z4vyjds&usqp=CAU"
            className="block w-full"
            alt="..."
          />
          <div className="absolute inset-x-[15%] bottom-5 hidden py-5 text-center text-white md:block">
            <h5 className="text-6xl font-bold">Traveling </h5>
            <p className="text-4xl">Điểm đến mong muốn của bạn</p>
          </div>
        </div>
      </div>
    </div>
  );
}
