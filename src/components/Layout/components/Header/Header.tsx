/* eslint-disable @next/next/no-html-link-for-pages */
/* eslint-disable import/no-cycle */

import Link from 'next/link';
import { useState } from 'react';
import { Button } from 'reactstrap';

import LoginHome from '../LoginModal/LoginModal';
import Modal from '../Modal';

export default function Header() {
  const [open, setOpen] = useState(false);
  return (
    <header>
      <nav className="border-gray-200 bg-black px-4 py-2.5 dark:bg-gray-800 lg:px-6">
        <div className="mx-auto flex max-w-screen-xl flex-wrap items-center justify-between">
          <Link href="/" className="flex items-center">
            <img
              src="https://flowbite.com/docs/images/logo.svg"
              className="mr-3 h-6 sm:h-9"
              alt=""
            />
            <span className="self-center whitespace-nowrap text-xl font-semibold text-white">
              TRAVELING
            </span>
          </Link>
          <div className="flex items-center lg:order-2">
            <Button
              onClick={() => {
                setOpen(true);
              }}
              href="#"
              className="mr-2 rounded-lg border-2 border-solid  border-white px-4 py-2 text-sm font-medium text-white hover:bg-slate-300 hover:text-black focus:outline-none focus:ring-4 focus:ring-gray-300 dark:hover:bg-gray-700 dark:focus:ring-gray-800 lg:px-5 lg:py-2.5"
            >
              Login
            </Button>
            <button
              data-collapse-toggle="mobile-menu-2"
              type="button"
              className="ml-1 inline-flex items-center rounded-lg p-2 text-sm text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600 lg:hidden"
              aria-controls="mobile-menu-2"
              aria-expanded="false"
            >
              <svg
                className="h-6 w-6"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                  clipRule="evenodd"
                />
              </svg>
              <svg
                className="hidden h-6 w-6"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                  clipRule="evenodd"
                />
              </svg>
            </button>
          </div>
          <div
            className="hidden w-full items-center justify-between lg:order-1 lg:flex lg:w-auto"
            id="mobile-menu-2"
          >
            <ul className="mt-4 flex flex-col font-medium lg:mt-0 lg:flex-row lg:space-x-8">
              <li>
                <Link
                  href="/"
                  className="block rounded py-2 pl-3 pr-4 text-white "
                  aria-current="page"
                >
                  Trang chủ
                </Link>
              </li>
              <li>
                <Link
                  href="/search-tour"
                  className="block py-2 pl-3 pr-4  text-white "
                >
                  Điểm đến
                </Link>
              </li>
              <li>
                <Link href="/news" className="block py-2 pl-3 pr-4 text-white ">
                  Tin tức
                </Link>
              </li>
              <li>
                <Link
                  href="/contact"
                  className="block py-2 pl-3 pr-4 text-white "
                >
                  Liên hệ
                </Link>
              </li>
              <li>
                <Link
                  href="/aboutUs"
                  className="block py-2 pl-3 pr-4 text-white "
                >
                  Về chúng tôi
                </Link>
              </li>
            </ul>
          </div>
        </div>
        <Modal open={open} setOpen={setOpen}>
          {' '}
          <LoginHome />{' '}
        </Modal>
      </nav>
    </header>
  );
}
