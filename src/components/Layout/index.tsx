import type { ReactNode } from 'react';

import { Footer, Header } from './components';

export default function Layout({ children }: { children: ReactNode }) {
  return (
    <>
      <Header />
      {children}
      <Footer />
    </>
  );
}
