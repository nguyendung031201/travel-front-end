export const PATH_ROUTER = {
  dashboard: '/admin',
  common: '/admin/common',
  blog: '/admin/blog',
  booking: '/admin/booking',
  user: '/admin/user',
  banners: '/admin/bannera',
};

export const LIST_SIDE_BAR = [
  {
    name: '',
    link: PATH_ROUTER.common,
  },
  {
    name: 'Blog',
    link: PATH_ROUTER.blog,
  },
  {
    name: 'Booking',
    link: PATH_ROUTER.booking,
  },
  {
    name: 'User',
    link: PATH_ROUTER.user,
  },
  {
    name: 'Banner ',
    link: PATH_ROUTER.banners,
  },
  {
    name: 'Đăng xuất',
    link: PATH_ROUTER.dashboard,
  },
];
