import Layout from '@/components/Layout';

import { Contacts } from './components';

export default function ContactPage() {
  return (
    <Layout>
      <Contacts />
    </Layout>
  );
}
