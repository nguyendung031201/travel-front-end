import Layout from '@/components/Layout';

import News from './components/New/News';

export default function NewPage() {
  return (
    <Layout>
      <News />
    </Layout>
  );
}
