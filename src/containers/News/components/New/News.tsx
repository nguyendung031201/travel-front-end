/* eslint-disable react/no-unescaped-entities */
import Link from 'next/link';

import { Banner } from '@/components/Layout/components';
import { InstaFollow } from '@/containers/Home/components';

/* eslint-disable react/jsx-no-undef */
export default function News() {
  return (
    <div className="">
      <div className="">
        <Banner />
      </div>
      <div className="container mx-auto grid grid-cols-3 gap-4">
        <div className="col-span-2 m-5">
          <div className="text-center text-5xl font-bold">
            <h1>Những tin tức mới dành cho bạn</h1>
          </div>
          <div className="m-5">
            <Link
              href="https://vnexpress.net/du-lich-ngay-le-met-nhieu-hon-vui-4601191.html"
              className="flex flex-col items-center rounded-lg border border-gray-200 bg-white shadow hover:bg-gray-100 dark:border-gray-700 dark:bg-gray-800 dark:hover:bg-gray-700 md:w-full md:flex-row"
            >
              <img
                className="h-96 w-full rounded-t-lg object-cover md:h-auto md:w-48 md:rounded-none md:rounded-l-lg"
                src="https://i1-dulich.vnecdn.net/2023/05/04/tau1-1683198675-3862-1683198705.jpg?w=240&h=144&q=100&dpr=1&fit=crop&s=hY2O_KOM6rFjUcsUBvtLQg"
                alt=""
              />
              <div className="flex flex-col justify-between p-4 leading-normal">
                <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                  Du lịch nghỉ lễ "mệt nhiều hơn vui"
                </h5>
                <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">
                  Anh Tuân Phạm, Hà Nội, và gia đình "không thấy vui" sau chuyến
                  đi Sa Pa dịp 30/4 vì một số nơi "tận thu" mà bỏ qua chất lượng
                  dịch vụ.
                </p>
              </div>
            </Link>
          </div>
          <div className="m-5">
            <Link
              href="https://video.vnexpress.net/tin-tuc/du-lich/nha-gom-hinh-ban-xoay-hut-khach-trai-nghiem-4600725.html"
              className="flex flex-col items-center rounded-lg border border-gray-200 bg-white shadow hover:bg-gray-100 dark:border-gray-700 dark:bg-gray-800 dark:hover:bg-gray-700 md:w-full md:flex-row"
            >
              <img
                className="h-96 w-full rounded-t-lg object-cover md:h-auto md:w-48 md:rounded-none md:rounded-l-lg"
                src="https://i1-vnexpress.vnecdn.net/2023/05/03/nhgm-1683105454-7276-1683105493.jpg?w=240&h=144&q=100&dpr=1&fit=crop&s=LjYsZ29JRCZ26Xfk8khwZA"
                alt=""
              />
              <div className="flex flex-col justify-between p-4 leading-normal">
                <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                  Nhà gốm hình bàn xoay thu hút khách trải nghiệm
                </h5>
                <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">
                  HÀ NỘI Hàng chục nghìn người tham quan, trải nghiệm làm gốm
                  tại bảo tàng gốm hình bàn xoay ở huyện Gia Lâm.
                </p>
              </div>
            </Link>
          </div>
          <div className="m-5">
            <Link
              href="https://vnexpress.net/phong-cach-du-lich-cua-the-he-z-4597661.html"
              className="flex flex-col items-center rounded-lg border border-gray-200 bg-white shadow hover:bg-gray-100 dark:border-gray-700 dark:bg-gray-800 dark:hover:bg-gray-700 md:w-full md:flex-row"
            >
              <img
                className="h-96 w-full rounded-t-lg object-cover md:h-auto md:w-48 md:rounded-none md:rounded-l-lg"
                src="https://i1-dulich.vnecdn.net/2023/05/04/1-8761-1682390428-1683168281-1846-1683168284.jpg?w=240&h=144&q=100&dpr=1&fit=crop&s=hvx2mczyoAoLRdXaYY1RfA"
                alt=""
              />
              <div className="flex flex-col justify-between p-4 leading-normal">
                <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                  Phong cách du lịch của thế hệ Z
                </h5>
                <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">
                  Bất chấp suy thoái kinh tế, thế hệ Z vẫn chi tiền cho các
                  chuyến đi và ngày càng du lịch nhiều hơn.
                </p>
              </div>
            </Link>
          </div>
          <div className="m-5">
            <Link
              href="https://vnexpress.net/du-khach-mat-nhieu-gio-lam-thu-tuc-qua-cua-khau-mong-cai-4600425.html"
              className="flex flex-col items-center rounded-lg border border-gray-200 bg-white shadow hover:bg-gray-100 dark:border-gray-700 dark:bg-gray-800 dark:hover:bg-gray-700 md:w-full md:flex-row"
            >
              <img
                className="h-96 w-full rounded-t-lg object-cover md:h-auto md:w-48 md:rounded-none md:rounded-l-lg"
                src="https://i1-vnexpress.vnecdn.net/2023/05/02/mong-cai1-1683017826-8532-1683018011.jpg?w=240&h=144&q=100&dpr=1&fit=crop&s=ZLTwiTV3qzUEjOBM_HaP1g"
                alt=""
              />
              <div className="flex flex-col justify-between p-4 leading-normal">
                <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                  Du khách mất nhiều giờ làm thủ tục qua cửa khẩu Móng Cái
                </h5>
                <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">
                  QUẢNG NINH Khách Việt mất 3-6 giờ làm thủ tục xuất nhập cảnh
                  tại Móng Cái do một số quy định phòng dịch của Trung Quốc và
                  lượng khách tăng đột biến dịp lễ.
                </p>
              </div>
            </Link>
          </div>
          <div className="m-5">
            <Link
              href="https://vnexpress.net/khach-trung-quoc-den-viet-nam-tang-manh-4600030.html"
              className="flex flex-col items-center rounded-lg border border-gray-200 bg-white shadow hover:bg-gray-100 dark:border-gray-700 dark:bg-gray-800 dark:hover:bg-gray-700 md:w-full md:flex-row"
            >
              <img
                className="h-96 w-full rounded-t-lg object-cover md:h-auto md:w-48 md:rounded-none md:rounded-l-lg"
                src="https://i1-dulich.vnecdn.net/2023/04/30/1-9675-1682847102.jpg?w=240&h=144&q=100&dpr=1&fit=crop&s=x3EeIlz8eBX6m70R0w-BUw"
                alt=""
              />
              <div className="flex flex-col justify-between p-4 leading-normal">
                <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                  Khách Trung Quốc đến Việt Nam tăng mạnh
                </h5>
                <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">
                  Tháng 4, lượng khách Trung Quốc tăng trưởng cao nhất trong số
                  các thị trường khách quốc tế tới Việt Nam.
                </p>
              </div>
            </Link>
          </div>
          <div className="m-5">
            <Link
              href="https://vnexpress.net/hang-nghin-du-khach-den-ba-na-cong-vien-chau-a-4600421.html"
              className="flex flex-col items-center rounded-lg border border-gray-200 bg-white shadow hover:bg-gray-100 dark:border-gray-700 dark:bg-gray-800 dark:hover:bg-gray-700 md:w-full md:flex-row"
            >
              <img
                className="h-96 w-full rounded-t-lg object-cover md:h-auto md:w-48 md:rounded-none md:rounded-l-lg"
                src="https://i1-dulich.vnecdn.net/2023/05/02/image1805480662extractword6out-389370-1683001935.jpeg?w=240&h=144&q=100&dpr=1&fit=crop&s=7qU011vMmOUL3QmwMv1byg"
                alt=""
              />
              <div className="flex flex-col justify-between p-4 leading-normal">
                <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                  Hàng nghìn du khách đến Bà Nà, Công viên Châu Á
                </h5>
                <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">
                  ĐÀ NẴNG - Hoạt động vui chơi, giải trí đa dạng tại Sun World
                  Ba Na Hills, Công viên Châu Á thu hút hàng nghìn người đến
                  trải nghiệm dịp 30/4-1/5.
                </p>
              </div>
            </Link>
          </div>
        </div>
        <div className="m-5">
          <div className="text-2xl font-bold text-red-600">
            <h1>Tin hot nhất</h1>
          </div>
          <div className="w-full">
            <div className="">
              <Link
                href="https://vnexpress.net/du-khach-mat-nhieu-gio-lam-thu-tuc-qua-cua-khau-mong-cai-4600425.html"
                className="flex flex-col items-center rounded-lg border border-gray-200 bg-white shadow hover:bg-gray-100 dark:border-gray-700 dark:bg-gray-800 dark:hover:bg-gray-700 md:w-full md:flex-row"
              >
                <img
                  className="h-full w-full rounded-t-lg object-cover md:h-auto md:w-48 md:rounded-none md:rounded-l-lg"
                  src="https://i1-vnexpress.vnecdn.net/2023/05/02/mong-cai1-1683017826-8532-1683018011.jpg?w=240&h=144&q=100&dpr=1&fit=crop&s=ZLTwiTV3qzUEjOBM_HaP1g"
                  alt=""
                />
                <div className="flex flex-col justify-between p-4 leading-normal">
                  <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                    Du khách mất nhiều giờ làm thủ tục qua cửa khẩu Móng Cái
                  </h5>
                </div>
              </Link>
            </div>
            <div className="">
              <Link
                href="https://video.vnexpress.net/tin-tuc/du-lich/trai-nghiem-cuoi-ngua-tren-thao-nguyen-4600646.html"
                className="flex flex-col items-center rounded-lg border border-gray-200 bg-white shadow hover:bg-gray-100 dark:border-gray-700 dark:bg-gray-800 dark:hover:bg-gray-700 md:w-full md:flex-row"
              >
                <img
                  className="h-full w-full rounded-t-lg object-cover md:h-auto md:w-48 md:rounded-none md:rounded-l-lg"
                  src="https://i1-vnexpress.vnecdn.net/2023/05/03/Dung00011504Still004-168308521-1360-9753-1683085215.jpg?w=120&h=72&q=100&dpr=1&fit=crop&s=-jfrFbJZn4DdjKJt7vK00w"
                  alt=""
                />
                <div className="flex flex-col justify-between p-4 leading-normal">
                  <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                    Trải nghiệm cưỡi ngựa trên thảo nguyên
                  </h5>
                </div>
              </Link>
            </div>
            <div className="">
              <Link
                href="https://video.vnexpress.net/tin-tuc/du-lich/nguoi-dan-xep-hang-thuong-thuc-tai-le-hoi-banh-mi-4587376.html"
                className="flex flex-col items-center rounded-lg border border-gray-200 bg-white shadow hover:bg-gray-100 dark:border-gray-700 dark:bg-gray-800 dark:hover:bg-gray-700 md:w-full md:flex-row"
              >
                <img
                  className="h-full w-full rounded-t-lg object-cover md:h-auto md:w-48 md:rounded-none md:rounded-l-lg"
                  src="https://i1-vnexpress.vnecdn.net/2023/03/30/336648804180487834785997347866-2410-2743-1680162146.jpg?w=120&h=72&q=100&dpr=1&fit=crop&s=ccJD9yruuw7iJ8gREzLxZg"
                  alt=""
                />
                <div className="flex flex-col justify-between p-4 leading-normal">
                  <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                    Người dân xếp hàng thưởng thức tại lễ hội bánh mì
                  </h5>
                </div>
              </Link>
            </div>
          </div>
        </div>
      </div>
      <InstaFollow />
    </div>
  );
}
