/* eslint-disable no-underscore-dangle */
/* eslint-disable tailwindcss/no-custom-classname */
import { useEffect, useState } from 'react';
import { toast } from 'react-toastify';

import { EditIcon, RemoveIcon } from '@/components/Layout/components/svg';
import type { IIteamTour } from '@/services/tour';
import servicesTour from '@/services/tour';

import FormEdit from './components/FormEdit/FormEdit';

export default function AdminTour() {
  const [tours, setTours] = useState<IIteamTour[]>([]);
  const getTours = async () => {
    const res = await servicesTour.getAll();
    if (res.data.tour) {
      setTours(res.data.tour);
    }
  };
  const [showModal, setShowModal] = useState(false);
  const [currentTourEdit, setCurrentTourEdit] = useState<
    IIteamTour | undefined
  >();

  useEffect(() => {
    getTours();
  }, []);

  const onRemoveTour = async (idTour: string | number) => {
    const res = await servicesTour.removeM(idTour);
    if (res.data.success) {
      toast.success('Đã xóa bản ghi');
      getTours();
      return;
    }
    toast.error('Xảy ra lỗi!');
  };

  return (
    <>
      <div className="text-center text-3xl font-bold">
        <p>Trình Quản Lý TOUR</p>
        <hr />
      </div>
      <div className="">
        <p className="text-xl">Danh sách ảnh Banner</p>

        <button
          className="mb-1 mr-1 rounded
      bg-blue-200 px-6 py-3 font-bold text-black shadow outline-none hover:shadow-lg focus:outline-none active:bg-blue-500"
          type="button"
          onClick={() => {
            setShowModal(true);
            setCurrentTourEdit(undefined);
          }}
        >
          Thêm ảnh
        </button>
        {showModal ? (
          <FormEdit
            reload={getTours}
            setShowModal={setShowModal}
            dataTour={currentTourEdit}
          />
        ) : null}
      </div>
      <table className="mt-5 w-full text-left text-sm text-gray-500 dark:text-gray-400">
        <thead className="bg-gray-50 text-xs uppercase text-gray-700 dark:bg-gray-700 dark:text-gray-400">
          <tr>
            <th scope="col" className="px-6 py-3">
              Tiêu đề
            </th>
            <th scope="col" className="px-6 py-3">
              Ảnh
            </th>
            <th scope="col" className="px-6 py-3">
              Giá
            </th>
            <th scope="col" className="px-6 py-3">
              Số người
            </th>
            <th scope="col" className="px-6 py-3">
              Địa điểm
            </th>
            <th scope="col" className="px-6 py-3">
              Thời gian
            </th>
            <th scope="col" className="px-6 py-3">
              Nội dung
            </th>
            <th scope="col" className="px-6 py-3">
              Chi tiết
            </th>
            <th scope="col" className="px-6 py-3">
              Hành động
            </th>
          </tr>
        </thead>
        <tbody>
          {tours.map((tour) => (
            <tr
              key={tour._id}
              className="border-b bg-white dark:border-gray-700 dark:bg-gray-800"
            >
              <th
                scope="row"
                className="whitespace-nowrap px-6 py-4 font-medium text-gray-900 dark:text-white"
              >
                {tour.title}
              </th>
              <th
                scope="row"
                className="whitespace-nowrap px-6 py-4 font-medium text-gray-900 dark:text-white"
              >
                <img src={tour.image} alt="" />
              </th>
              <th
                scope="row"
                className="whitespace-nowrap px-6 py-4 font-medium text-gray-900 dark:text-white"
              >
                {tour.price}
              </th>

              <th
                scope="row"
                className="whitespace-nowrap px-6 py-4 font-medium text-gray-900 dark:text-white"
              >
                {tour.limitPeople}
              </th>
              <th
                scope="row"
                className="whitespace-nowrap px-6 py-4 font-medium text-gray-900 dark:text-white"
              >
                {tour.location}
              </th>
              <th
                scope="row"
                className="whitespace-nowrap px-6 py-4 font-medium text-gray-900 dark:text-white"
              >
                {tour.timeDate}
              </th>
              <th
                scope="row"
                className="max-w-[300px] truncate whitespace-nowrap px-6 py-4 font-medium text-gray-900 dark:text-white"
              >
                {tour.content}
              </th>
              <th
                scope="row"
                className="max-w-[350px] truncate whitespace-nowrap  font-medium text-gray-900 dark:text-white"
              >
                {tour.tourDetail}
              </th>
              <td className="px-6 py-4">
                <div className="grid grid-cols-1">
                  <EditIcon
                    className="h-6 w-6 cursor-pointer text-red-500"
                    type="button"
                    onClick={() => {
                      setCurrentTourEdit(tour);
                      setShowModal(true);
                    }}
                  />
                  <RemoveIcon
                    className="h-6 w-6 cursor-pointer text-red-500"
                    onClick={() => onRemoveTour(tour._id)}
                  />
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
}
