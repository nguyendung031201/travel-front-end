/* eslint-disable no-underscore-dangle */
import { useState } from 'react';
import { toast } from 'react-toastify';

import type { IIteamTour } from '@/services/tour';
import servicesTour from '@/services/tour';
// import servicesUpload from '@/services/upload';

interface IProps {
  setShowModal: (e: boolean) => void;
  dataTour?: IIteamTour;
  reload: () => Promise<void>;
}

export default function FormEdit({ setShowModal, dataTour, reload }: IProps) {
  const [title, setTitle] = useState<string>(dataTour?.title || '');
  const [image, setImage] = useState<string>(dataTour?.image || '');
  const [price, setPrice] = useState<string>(dataTour?.price || '');
  const [limitPeople, setLimitPeople] = useState<string>(
    dataTour?.limitPeople || ''
  );
  const [location, setLocation] = useState<string>(dataTour?.location || '');
  const [timeDate, setTimeDate] = useState<string>(dataTour?.timeDate || '');
  const [content, setContent] = useState<string>(dataTour?.content || '');
  const [tourDetail, setTourDetail] = useState<string>(
    dataTour?.tourDetail || ''
  );

  const handleSave = async () => {
    if (dataTour?._id) {
      const dataTourNew: IIteamTour = {
        _id: dataTour._id,
        title,
        image,
        price,
        limitPeople,
        location,
        timeDate,
        content,
        tourDetail,
      };
      const resUpdate = await servicesTour.update(dataTourNew);
      if (resUpdate.data.success) {
        reload();
        setShowModal(false);
        toast.success('Update thành công');
      } else {
        toast.error('LLỗiLỗi');
      }
      return;
    }
    const dataTourNew: IIteamTour = {
      _id: '',
      title,
      image,
      price,
      limitPeople,
      location,
      timeDate,
      content,
      tourDetail,
    };
    const resSave = await servicesTour.create(dataTourNew);
    if (resSave.data.success) {
      reload();
      setShowModal(false);
      setShowModal(false);
      toast.success('Update thành công');
    } else {
      toast.error('LLỗiLỗi');
    }
  };

  return (
    <>
      <div className=" max-h-full w-full ">
        <div className="relative mx-auto my-6 w-auto max-w-3xl">
          <div className="relative flex w-full flex-col rounded-lg border-0 bg-white shadow-lg outline-none focus:outline-none">
            <div className="flex items-start justify-between rounded-t border-b border-solid border-gray-300 p-5 ">
              <h3 className="text-3xl font-semibold">Thêm tour của bạn</h3>
              <button
                className="float-right border-0 bg-transparent text-black"
                onClick={() => setShowModal(false)}
              >
                <span className="block h-6 w-6 rounded-full bg-gray-400 py-0 text-xl text-black">
                  x
                </span>
              </button>
            </div>
            <div className="relative flex-auto p-6">
              <form className="w-full rounded bg-gray-200 px-8 pb-8 pt-6 shadow-md">
                <label className="mb-1 block text-lg  text-black">
                  Tiêu đề
                </label>
                <input
                  value={title}
                  onChange={(e) => {
                    setTitle(e.target.value);
                  }}
                  className="w-full appearance-none rounded border px-1 py-2 text-black shadow"
                />
                <label className="mb-1 block text-lg  text-black">Ảnh</label>
                <input
                  value={image}
                  onChange={(e) => {
                    setImage(e.target.value);
                  }}
                  className="w-full appearance-none rounded border px-1 py-2 text-black shadow"
                />
                <label className="mb-1 block text-lg  text-black">Giá</label>
                <input
                  value={price}
                  onChange={(e) => {
                    setPrice(e.target.value);
                  }}
                  className="w-full appearance-none rounded border px-1 py-2 text-black shadow"
                />
                <label className="mb-1 block text-lg  text-black">
                  Số người
                </label>
                <input
                  value={limitPeople}
                  onChange={(e) => {
                    setLimitPeople(e.target.value);
                  }}
                  className="w-full appearance-none rounded border px-1 py-2 text-black shadow"
                />
                <label className="mb-1 block text-lg  text-black">
                  Địa điểm
                </label>
                <input
                  value={location}
                  onChange={(e) => {
                    setLocation(e.target.value);
                  }}
                  className="w-full appearance-none rounded border px-1 py-2 text-black shadow"
                />
                <label className="mb-1 block text-lg  text-black">
                  Thời gian
                </label>
                <input
                  value={timeDate}
                  onChange={(e) => {
                    setTimeDate(e.target.value);
                  }}
                  className="w-full appearance-none rounded border px-1 py-2 text-black shadow"
                />
                <label className="mb-1 block text-lg  text-black">
                  Nội dung
                </label>
                <input
                  value={content}
                  onChange={(e) => {
                    setContent(e.target.value);
                  }}
                  className="w-full appearance-none rounded border px-1 py-2 text-black shadow"
                />
                <label className="mb-1 block text-lg  text-black">
                  Chi tiết
                </label>
                <input
                  value={tourDetail}
                  onChange={(e) => {
                    setTourDetail(e.target.value);
                  }}
                  className="w-full appearance-none rounded border px-1 py-2 text-black shadow"
                />
              </form>
            </div>
            <div className="flex items-center justify-end rounded-b border-t border-solid p-6">
              <button
                className="mb-1 mr-1 px-6 py-2 text-sm font-bold uppercase text-red-500 outline-none focus:outline-none"
                type="button"
                onClick={() => setShowModal(false)}
              >
                Đóng
              </button>
              <button
                className="mb-1 mr-1 rounded bg-yellow-500 px-6 py-3 text-sm font-bold uppercase text-white shadow outline-none hover:shadow-lg focus:outline-none active:bg-yellow-700"
                type="button"
                onClick={handleSave}
              >
                Lưu
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
