/* eslint-disable no-underscore-dangle */
import { useState } from 'react';
import { toast } from 'react-toastify';

import type { IItemUser } from '@/services/user';
import servicesUser from '@/services/user';
// import servicesUpload from '@/services/upload';

interface IProps {
  setShowModal: (e: boolean) => void;
  dataUser?: IItemUser;
  reload: () => Promise<void>;
}

export default function FormEdit({ setShowModal, dataUser, reload }: IProps) {
  const [username, setUsername] = useState<string>(dataUser?.username || '');
  const [password, setPassword] = useState<string>(dataUser?.password || '');
  const [role, setRole] = useState<string>(dataUser?.role || '');

  const handleSave = async () => {
    if (dataUser?._id) {
      const dataUserNew: IItemUser = {
        _id: dataUser._id,
        username,
        password,
        role,
      };
      const resUpdate = await servicesUser.update(dataUserNew);
      if (resUpdate.data.success) {
        reload();
        setShowModal(false);
        toast.success('Update thành công');
      } else {
        toast.error('Lỗi');
      }
      return;
    }
    const dataUserNew: IItemUser = {
      _id: '',
      username,
      password,
      role,
    };
    const resSave = await servicesUser.create(dataUserNew);
    if (resSave.data.success) {
      reload();
      setShowModal(false);
      setShowModal(false);
      toast.success('Update thành công');
    } else {
      toast.error('LLỗiLỗi');
    }
  };

  return (
    <>
      <div className=" max-h-full w-full ">
        <div className="relative mx-auto my-6 w-auto max-w-3xl">
          <div className="relative flex w-full flex-col rounded-lg border-0 bg-white shadow-lg outline-none focus:outline-none">
            <div className="flex items-start justify-between rounded-t border-b border-solid border-gray-300 p-5 ">
              <h3 className="text-3xl font-semibold">Thêm </h3>
              <button
                className="float-right border-0 bg-transparent text-black"
                onClick={() => setShowModal(false)}
              >
                <span className="block h-6 w-6 rounded-full bg-gray-400 py-0 text-xl text-black">
                  x
                </span>
              </button>
            </div>
            <div className="relative flex-auto p-6">
              <form className="w-full rounded bg-gray-200 px-8 pb-8 pt-6 shadow-md">
                <label className="mb-1 block text-lg  text-black">
                  Tên đăng nhập
                </label>
                <input
                  value={username}
                  onChange={(e) => {
                    setUsername(e.target.value);
                  }}
                  className="w-full appearance-none rounded border px-1 py-2 text-black shadow"
                />
                <label className="mb-1 block text-lg  text-black">
                  Mật Khẩu
                </label>
                <input
                  value={password}
                  onChange={(e) => {
                    setPassword(e.target.value);
                  }}
                  className="w-full appearance-none rounded border px-1 py-2 text-black shadow"
                />
                <label className="mb-1 block text-lg  text-black">
                  Quyền ( 1 = admin và 2 = user )
                </label>
                <input
                  value={role}
                  onChange={(e) => {
                    setRole(e.target.value);
                  }}
                  className="w-full appearance-none rounded border px-1 py-2 text-black shadow"
                />
              </form>
            </div>
            <div className="flex items-center justify-end rounded-b border-t border-solid p-6">
              <button
                className="mb-1 mr-1 px-6 py-2 text-sm font-bold uppercase text-red-500 outline-none focus:outline-none"
                type="button"
                onClick={() => setShowModal(false)}
              >
                Đóng
              </button>
              <button
                className="mb-1 mr-1 rounded bg-yellow-500 px-6 py-3 text-sm font-bold uppercase text-white shadow outline-none hover:shadow-lg focus:outline-none active:bg-yellow-700"
                type="button"
                onClick={handleSave}
              >
                Lưu
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
