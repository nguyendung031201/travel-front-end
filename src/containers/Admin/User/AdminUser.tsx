/* eslint-disable no-underscore-dangle */
/* eslint-disable import/no-named-as-default */
import { useEffect, useState } from 'react';
import { toast } from 'react-toastify';

import { EditIcon, RemoveIcon } from '@/components/Layout/components/svg';
import type { IItemUser } from '@/services/user';
import servicesUser from '@/services/user';

import FormEdit from './components/FormEdit/FormEdit';

/* eslint-disable tailwindcss/no-custom-classname */
export default function AdminUser() {
  const [users, setUsers] = useState<IItemUser[]>([]);
  const getUsers = async () => {
    const res = await servicesUser.getAll();
    if (res.data.user) {
      setUsers(res.data.user);
    }
  };
  const [showModal, setShowModal] = useState(false);
  const [currentUserEdit, setCurrentUserEdit] = useState<
    IItemUser | undefined
  >();

  useEffect(() => {
    getUsers();
  }, []);

  const onRemoveUser = async (idUser: string | number) => {
    const res = await servicesUser.removeM(idUser);
    if (res.data.success) {
      toast.success('Đã xóa bản ghi');
      getUsers();
      return;
    }
    toast.error('Xảy ra lỗi!');
  };

  return (
    <>
      <div className="text-center text-3xl font-bold">
        <p>Trình Quản Lý User</p>
        <hr />
      </div>
      <div className="">
        <p className="text-xl">Danh sách ảnh User</p>

        <button
          className="mb-1 mr-1 rounded
      bg-blue-200 px-6 py-3 font-bold text-black shadow outline-none hover:shadow-lg focus:outline-none active:bg-blue-500"
          type="button"
          onClick={() => {
            setShowModal(true);
            setCurrentUserEdit(undefined);
          }}
        >
          Thêm
        </button>
        {showModal ? (
          <FormEdit
            reload={getUsers}
            setShowModal={setShowModal}
            dataUser={currentUserEdit}
          />
        ) : null}
      </div>
      <table className="mt-5 w-full text-left text-sm text-gray-500 dark:text-gray-400">
        <thead className="bg-gray-50 text-xs uppercase text-gray-700 dark:bg-gray-700 dark:text-gray-400">
          <tr>
            <th scope="col" className="px-6 py-3">
              Username
            </th>
            <th scope="col" className="px-6 py-3">
              Password
            </th>
            <th scope="col" className="px-6 py-3">
              Role
            </th>
            <th scope="col" className="px-6 py-3">
              Hành động
            </th>
          </tr>
        </thead>
        <tbody>
          {users.map((user) => (
            <tr key={user._id}>
              <td className="px-6 py-3">{user.username}</td>
              <td className="px-6 py-3">{user.password}</td>
              <td className="px-6 py-3">{user.role}</td>
              <td className="px-6 py-3 ">
                <div className="grid grid-cols-1">
                  <EditIcon
                    className="h-6 w-6 cursor-pointer text-red-500"
                    type="button"
                    onClick={() => {
                      setCurrentUserEdit(user);
                      setShowModal(true);
                    }}
                  />
                  <RemoveIcon
                    className="h-6 w-6 cursor-pointer text-red-500"
                    onClick={() => onRemoveUser(user._id)}
                  />
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
}
