/* eslint-disable no-underscore-dangle */
/* eslint-disable import/no-named-as-default */
import { useEffect, useState } from 'react';
import { toast } from 'react-toastify';

import { RemoveIcon } from '@/components/Layout/components/svg';
import type { IItemFeedback } from '@/services/feedback';
import servicesFeedback from '@/services/feedback';

/* eslint-disable tailwindcss/no-custom-classname */
export default function AdminFeedBack() {
  const [feedbacks, setFeedBacks] = useState<IItemFeedback[]>([]);
  const getFeedBacks = async () => {
    const res = await servicesFeedback.getAll();
    if (res.data.feedback) {
      setFeedBacks(res.data.feedback);
    }
  };
  // const [showModal, setShowModal] = useState(false);
  // const [currentBannerEdit, setCurrentBannerEdit] = useState<
  //   IItemBanner | undefined
  // >();

  useEffect(() => {
    getFeedBacks();
  }, []);

  const onRemoveFeedBack = async (idFeedBack: string | number) => {
    const res = await servicesFeedback.removeM(idFeedBack);
    if (res.data.success) {
      toast.success('Đã xóa bản ghi');
      getFeedBacks();
      return;
    }
    toast.error('Xảy ra lỗi!');
  };

  return (
    <>
      <div className="text-center text-3xl font-bold">
        <p>Trình Quản Lý FEEDBACK</p>
        <hr />
      </div>
      <table className="mt-5 w-full text-left text-sm text-gray-500 dark:text-gray-400">
        <thead className="bg-gray-50 text-xs uppercase text-gray-700 dark:bg-gray-700 dark:text-gray-400">
          <tr>
            <th scope="col" className="px-6 py-3">
              First Name
            </th>
            <th scope="col" className="px-6 py-3">
              Last Name
            </th>
            <th scope="col" className="px-6 py-3">
              email
            </th>
            <th scope="col" className="px-6 py-3">
              Feedback
            </th>
            <th scope="col" className="px-6 py-3">
              Hành động
            </th>
          </tr>
        </thead>
        <tbody>
          {feedbacks.map((feedback) => (
            <tr key={feedback._id}>
              <td className="px-6 py-3">{feedback.firstName}</td>
              <td className="px-6 py-3">{feedback.lastName}</td>
              <td className="px-6 py-3">{feedback.email}</td>
              <td className="px-6 py-3">{feedback.feedBack}</td>
              <td className="px-6 py-3 ">
                <div className="grid grid-cols-1">
                  {/* <EditIcon
                    className="h-6 w-6 cursor-pointer text-red-500"
                    type="button"
                    onClick={() => {
                      setCurrentBannerEdit(feedback);
                      setShowModal(true);
                    }}
                  /> */}
                  <RemoveIcon
                    className="h-6 w-6 cursor-pointer text-red-500"
                    onClick={() => onRemoveFeedBack(feedback._id)}
                  />
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
}
