/* eslint-disable no-underscore-dangle */
/* eslint-disable import/no-named-as-default */
import { useEffect, useState } from 'react';
import { toast } from 'react-toastify';

import { EditIcon, RemoveIcon } from '@/components/Layout/components/svg';
import type { IItemBanner } from '@/services/banner';
import servicesBanner from '@/services/banner';

import FormEdit from './components/FormEdit/FormEdit';

/* eslint-disable tailwindcss/no-custom-classname */
export default function AdminBanner() {
  const [banners, setBanners] = useState<IItemBanner[]>([]);
  const getBanners = async () => {
    const res = await servicesBanner.getAll();
    if (res.data.banner) {
      setBanners(res.data.banner);
    }
  };
  const [showModal, setShowModal] = useState(false);
  const [currentBannerEdit, setCurrentBannerEdit] = useState<
    IItemBanner | undefined
  >();

  useEffect(() => {
    getBanners();
  }, []);

  const onRemoveBanner = async (idBanner: string | number) => {
    const res = await servicesBanner.removeM(idBanner);
    if (res.data.success) {
      toast.success('Đã xóa bản ghi');
      getBanners();
      return;
    }
    toast.error('Xảy ra lỗi!');
  };

  return (
    <>
      <div className="text-center text-3xl font-bold">
        <p>Trình Quản Lý BANNER</p>
        <hr />
      </div>
      <div className="">
        <p className="text-xl">Danh sách ảnh Banner</p>

        <button
          className="mb-1 mr-1 rounded
      bg-blue-200 px-6 py-3 font-bold text-black shadow outline-none hover:shadow-lg focus:outline-none active:bg-blue-500"
          type="button"
          onClick={() => {
            setShowModal(true);
            setCurrentBannerEdit(undefined);
          }}
        >
          Thêm ảnh
        </button>
        {showModal ? (
          <FormEdit
            reload={getBanners}
            setShowModal={setShowModal}
            dataBanner={currentBannerEdit}
          />
        ) : null}
      </div>
      <table className="mt-5 w-full text-left text-sm text-gray-500 dark:text-gray-400">
        <thead className="bg-gray-50 text-xs uppercase text-gray-700 dark:bg-gray-700 dark:text-gray-400">
          <tr>
            <th scope="col" className="px-6 py-3">
              Vị trí
            </th>
            <th scope="col" className="px-6 py-3">
              Ảnh
            </th>
            <th scope="col" className="px-6 py-3">
              Hành động
            </th>
          </tr>
        </thead>
        <tbody>
          {banners.map((banner) => (
            <tr key={banner._id}>
              <td className="px-6 py-3">{banner.number}</td>
              <td className="px-6 py-3">
                <img src={banner.image} alt="" className="px-6 py-4 " />
              </td>
              <td className="px-6 py-3 ">
                <div className="grid grid-cols-1">
                  <EditIcon
                    className="h-6 w-6 cursor-pointer text-red-500"
                    type="button"
                    onClick={() => {
                      setCurrentBannerEdit(banner);
                      setShowModal(true);
                    }}
                  />
                  <RemoveIcon
                    className="h-6 w-6 cursor-pointer text-red-500"
                    onClick={() => onRemoveBanner(banner._id)}
                  />
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
}
