/* eslint-disable no-underscore-dangle */
import { useState } from 'react';
import { toast } from 'react-toastify';

import type { IItemBanner } from '@/services/banner';
import servicesBanner from '@/services/banner';
// import servicesUpload from '@/services/upload';

interface IProps {
  setShowModal: (e: boolean) => void;
  dataBanner?: IItemBanner;
  reload: () => Promise<void>;
}

export default function FormEdit({ setShowModal, dataBanner, reload }: IProps) {
  const [number, setNumber] = useState<string>(
    String(dataBanner?.number || '')
  );
  const [image, setImage] = useState<string>(dataBanner?.image || '');

  const handleSave = async () => {
    if (dataBanner?._id) {
      const dataBannerNew: IItemBanner = {
        _id: dataBanner._id,
        number: +number,
        image,
      };
      const resUpdate = await servicesBanner.update(dataBannerNew);
      if (resUpdate.data.success) {
        reload();
        setShowModal(false);
        toast.success('Update thành công');
      } else {
        toast.error('LLỗiLỗi');
      }
      return;
    }
    const dataBannerNew: IItemBanner = {
      _id: '',
      number: +number,
      image,
    };
    const resSave = await servicesBanner.create(dataBannerNew);
    if (resSave.data.success) {
      reload();
      setShowModal(false);
      setShowModal(false);
      toast.success('Update thành công');
    } else {
      toast.error('LLỗiLỗi');
    }
  };
  console.log(handleSave);

  return (
    <>
      <div className=" max-h-full w-full ">
        <div className="relative mx-auto my-6 w-auto max-w-3xl">
          <div className="relative flex w-full flex-col rounded-lg border-0 bg-white shadow-lg outline-none focus:outline-none">
            <div className="flex items-start justify-between rounded-t border-b border-solid border-gray-300 p-5 ">
              <h3 className="text-3xl font-semibold">Thêm ảnh của bạn</h3>
              <button
                className="float-right border-0 bg-transparent text-black"
                onClick={() => setShowModal(false)}
              >
                <span className="block h-6 w-6 rounded-full bg-gray-400 py-0 text-xl text-black">
                  x
                </span>
              </button>
            </div>
            <div className="relative flex-auto p-6">
              <form className="w-full rounded bg-gray-200 px-8 pb-8 pt-6 shadow-md">
                <label className="mb-1 block text-lg  text-black">
                  Số thứ tự
                </label>
                <input
                  value={number}
                  onChange={(e) => {
                    setNumber(e.target.value);
                  }}
                  className="w-full appearance-none rounded border px-1 py-2 text-black shadow"
                />
                <label className="mb-1 block text-lg  text-black">Ảnh</label>
                <input
                  value={image}
                  onChange={(e) => {
                    setImage(e.target.value);
                  }}
                  // onChange={handleChange}
                  className="w-full appearance-none rounded border px-1 py-2 text-black shadow"
                />

                <p
                  className="mt-1 text-lg text-gray-500 dark:text-gray-300"
                  id="file_input_help"
                >
                  SVG, PNG, JPG or GIF (MAX. 800x400px).
                </p>
              </form>
            </div>
            <div className="flex items-center justify-end rounded-b border-t border-solid p-6">
              <button
                className="mb-1 mr-1 px-6 py-2 text-sm font-bold uppercase text-red-500 outline-none focus:outline-none"
                type="button"
                onClick={() => setShowModal(false)}
              >
                Đóng
              </button>
              <button
                className="mb-1 mr-1 rounded bg-yellow-500 px-6 py-3 text-sm font-bold uppercase text-white shadow outline-none hover:shadow-lg focus:outline-none active:bg-yellow-700"
                type="button"
                onClick={handleSave}
              >
                Lưu
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
