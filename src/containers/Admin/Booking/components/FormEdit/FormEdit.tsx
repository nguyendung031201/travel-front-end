/* eslint-disable no-underscore-dangle */
import { useState } from 'react';
import { toast } from 'react-toastify';

import type { IItemBooking } from '@/services/booking';
import servicesBooking from '@/services/booking';
// import servicesUpload from '@/services/upload';

interface IProps {
  setShowModal: (e: boolean) => void;
  dataBooking?: IItemBooking;
  reload: () => Promise<void>;
}

export default function FormEdit({
  setShowModal,
  dataBooking,
  reload,
}: IProps) {
  const [email, setEmail] = useState<string>(String(dataBooking?.email || ''));
  const [fullName, setFullName] = useState<string>(dataBooking?.fullName || '');
  const [startDate, setStartDate] = useState<string>(
    dataBooking?.startDate || ''
  );
  const [people, setPeople] = useState<string>(dataBooking?.people || '');
  const [phoneNumber, setPhoneNumber] = useState<string>(
    dataBooking?.phoneNumber || ''
  );
  const [note, setNote] = useState<string>(dataBooking?.note || '');
  const [tour, setTour] = useState<string>(dataBooking?.tour || '');
  const [status, setStatus] = useState<string>(dataBooking?.status || '');

  const handleSave = async () => {
    if (dataBooking?._id) {
      const dataBookingNew: IItemBooking = {
        _id: dataBooking._id,
        email,
        fullName,
        startDate,
        people,
        phoneNumber,
        note,
        tour,
        status,
      };
      const resUpdate = await servicesBooking.update(dataBookingNew);
      if (resUpdate.data.success) {
        reload();
        setShowModal(false);
        toast.success('Update thành công');
      } else {
        toast.error('LLỗiLỗi');
      }
      return;
    }
    const dataBookingNew: IItemBooking = {
      _id: '',
      email,
      fullName,
      startDate,
      people,
      phoneNumber,
      note,
      tour,
      status,
    };
    const resSave = await servicesBooking.create(dataBookingNew);
    if (resSave.data.success) {
      reload();
      setShowModal(false);
      setShowModal(false);
      toast.success('Update thành công');
    } else {
      toast.error('LLỗiLỗi');
    }
  };

  return (
    <>
      <div className=" max-h-full w-full ">
        <div className="relative mx-auto my-6 w-auto max-w-3xl">
          <div className="relative flex w-full flex-col rounded-lg border-0 bg-white shadow-lg outline-none focus:outline-none">
            <div className="flex items-start justify-between rounded-t border-b border-solid border-gray-300 p-5 ">
              <h3 className="text-3xl font-semibold">Thêm tour của bạn</h3>
              <button
                className="float-right border-0 bg-transparent text-black"
                onClick={() => setShowModal(false)}
              >
                <span className="block h-6 w-6 rounded-full bg-gray-400 py-0 text-xl text-black">
                  x
                </span>
              </button>
            </div>
            <div className="relative flex-auto p-6">
              <form className="w-full rounded bg-gray-200 px-8 pb-8 pt-6 shadow-md">
                <label className="mb-1 block text-lg  text-black">Email</label>
                <input
                  value={email}
                  onChange={(e) => {
                    setEmail(e.target.value);
                  }}
                  className="w-full appearance-none rounded border px-1 py-2 text-black shadow"
                />
                <label className="mb-1 block text-lg  text-black">
                  Tên khách hàng
                </label>
                <input
                  value={fullName}
                  onChange={(e) => {
                    setFullName(e.target.value);
                  }}
                  className="w-full appearance-none rounded border px-1 py-2 text-black shadow"
                />
                <label className="mb-1 block text-lg  text-black">
                  Ngày Khởi Hành
                </label>
                <input
                  value={startDate}
                  onChange={(e) => {
                    setStartDate(e.target.value);
                  }}
                  className="w-full appearance-none rounded border px-1 py-2 text-black shadow"
                />
                <label className="mb-1 block text-lg  text-black">
                  Số lượng người
                </label>
                <input
                  value={people}
                  onChange={(e) => {
                    setPeople(e.target.value);
                  }}
                  className="w-full appearance-none rounded border px-1 py-2 text-black shadow"
                />
                <label className="mb-1 block text-lg  text-black">
                  Số điện thoại
                </label>
                <input
                  value={phoneNumber}
                  onChange={(e) => {
                    setPhoneNumber(e.target.value);
                  }}
                  className="w-full appearance-none rounded border px-1 py-2 text-black shadow"
                />
                <label className="mb-1 block text-lg  text-black">
                  Ghi chú
                </label>
                <input
                  value={note}
                  onChange={(e) => {
                    setNote(e.target.value);
                  }}
                  className="w-full appearance-none rounded border px-1 py-2 text-black shadow"
                />
                <label className="mb-1 block text-lg  text-black">
                  Tour đi
                </label>
                <input
                  value={tour}
                  onChange={(e) => {
                    setTour(e.target.value);
                  }}
                  className="w-full appearance-none rounded border px-1 py-2 text-black shadow"
                />
                <label className="mb-1 block text-lg  text-black">
                  Trạng thái
                </label>
                <input
                  value={status}
                  onChange={(e) => {
                    setStatus(e.target.value);
                  }}
                  className="w-full appearance-none rounded border px-1 py-2 text-black shadow"
                />
              </form>
            </div>
            <div className="flex items-center justify-end rounded-b border-t border-solid p-6">
              <button
                className="mb-1 mr-1 px-6 py-2 text-sm font-bold uppercase text-red-500 outline-none focus:outline-none"
                type="button"
                onClick={() => setShowModal(false)}
              >
                Đóng
              </button>
              <button
                className="mb-1 mr-1 rounded bg-yellow-500 px-6 py-3 text-sm font-bold uppercase text-white shadow outline-none hover:shadow-lg focus:outline-none active:bg-yellow-700"
                type="button"
                onClick={handleSave}
              >
                Lưu
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
