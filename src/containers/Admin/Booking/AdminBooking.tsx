/* eslint-disable no-underscore-dangle */
/* eslint-disable tailwindcss/no-custom-classname */

import { useEffect, useState } from 'react';
import { toast } from 'react-toastify';

import { EditIcon, RemoveIcon } from '@/components/Layout/components/svg';
import type { IItemBooking } from '@/services/booking';
import servicesBooking from '@/services/booking';

import FormEdit from './components/FormEdit/FormEdit';

export default function Booking() {
  const [bookings, setBookings] = useState<IItemBooking[]>([]);
  const getBooking = async () => {
    const res = await servicesBooking.getAll();
    if (res.data.booking) {
      setBookings(res.data.booking);
    }
  };
  const [showModal, setShowModal] = useState(false);
  const [currentBookingEdit, setCurrentBookingEdit] = useState<
    IItemBooking | undefined
  >();

  useEffect(() => {
    getBooking();
  }, []);

  const onRemoveBooking = async (idBooking: string | number) => {
    const res = await servicesBooking.removeM(idBooking);
    if (res.data.success) {
      toast.success('Đã xóa bản ghi');
      getBooking();
      return;
    }
    toast.error('Xảy ra lỗi!');
  };
  return (
    <>
      <div className="text-center text-3xl font-bold">
        <p>Trình Quản Lý BOOKING</p>
        <hr />
      </div>
      <div className="">
        <p className="text-xl">Danh sách Booking</p>
        <button
          className="mb-1 mr-1 rounded
      bg-blue-200 px-6 py-3 font-bold text-black shadow outline-none hover:shadow-lg focus:outline-none active:bg-blue-500"
          type="button"
          onClick={() => {
            setShowModal(true);
            setCurrentBookingEdit(undefined);
          }}
        >
          Thêm mới
        </button>
        {showModal ? (
          <FormEdit
            reload={getBooking}
            setShowModal={setShowModal}
            dataBooking={currentBookingEdit}
          />
        ) : null}
      </div>
      <table className="mt-5 w-full text-left text-sm text-gray-500 dark:text-gray-400">
        <thead className="bg-gray-50 text-xs uppercase text-gray-700 dark:bg-gray-700 dark:text-gray-400">
          <tr>
            <th scope="col" className="px-6 py-3">
              Email
            </th>
            <th scope="col" className="px-6 py-3">
              Tên khách hàng
            </th>
            {/* <th scope="col" className="px-6 py-3">
              Ngày khách khởi hành
            </th> */}
            <th scope="col" className="px-6 py-3">
              số lượng người
            </th>
            <th scope="col" className="px-6 py-3">
              Số điện thoại
            </th>
            <th scope="col" className="px-6 py-3">
              Ghi chú
            </th>
            {/* <th scope="col" className="px-6 py-3">
              Tour đi
            </th> */}
            <th scope="col" className="px-6 py-3">
              Trạng thái
            </th>
            <th scope="col" className="px-6 py-3">
              Hành động
            </th>
          </tr>
        </thead>
        <tbody>
          {bookings.map((booking) => (
            <tr key={booking._id}>
              <td className="px-6 py-3">{booking.email}</td>
              <td className="px-6 py-3">{booking.fullName}</td>
              {/* <td className="px-6 py-3">{booking.startDate}</td> */}
              <td className="px-6 py-3">{booking.people}</td>
              <td className="px-6 py-3">{booking.phoneNumber}</td>
              <td className="px-6 py-3">{booking.note}</td>
              {/* <td className="px-6 py-3">{booking.tour}</td> */}
              <td className="px-6 py-3">
                <div className="flex items-center">
                  <input
                    id="link-checkbox"
                    type="checkbox"
                    className="h-4 w-4 rounded border-gray-300 bg-gray-100 text-blue-600 focus:ring-2 focus:ring-blue-500 dark:border-gray-600 dark:bg-gray-700 dark:ring-offset-gray-800 dark:focus:ring-blue-600"
                  />
                  <label
                    htmlFor="link-checkbox"
                    className="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300"
                  ></label>
                </div>
              </td>
              <td className="px-6 py-3 ">
                <div className="grid grid-cols-1">
                  <EditIcon
                    className="h-6 w-6 cursor-pointer text-red-500"
                    type="button"
                    onClick={() => {
                      setCurrentBookingEdit(booking);
                      setShowModal(true);
                    }}
                  />
                  <RemoveIcon
                    className="h-6 w-6 cursor-pointer text-red-500"
                    onClick={() => onRemoveBooking(booking._id)}
                  />
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
}
