import Layout from '@/components/Layout';
import type { IIteamTour } from '@/services/tour';

import { SearchTour } from './components';

export default function DestinationPage({ tours }: { tours: IIteamTour[] }) {
  return (
    <Layout>
      <SearchTour tours={tours} />
    </Layout>
  );
}
