/* eslint-disable no-console */
/* eslint-disable no-underscore-dangle */
/* eslint-disable tailwindcss/no-custom-classname */
/* eslint-disable tailwindcss/migration-from-tailwind-2 */

import { useState } from 'react';
import { Modal } from 'reactstrap';

import { Banner } from '@/components/Layout/components';
import { ModalDetailTour } from '@/containers/Tours/components';
import type { IIteamTour } from '@/services/tour';

export default function SearchTour({ tours }: { tours: IIteamTour[] }) {
  const [open, setOpen] = useState(false);
  const [currentTour, setCurrentTour] = useState<IIteamTour | null>(null);
  return (
    <div className="">
      <div className="">
        <Banner />
      </div>
      <div className="container w-auto">
        <div className="grid grid-cols-3 gap-4">
          <div className="ml-10 mt-5">
            <div className="text-center text-2xl font-bold text-black">
              <p>Tìm kiếm tour</p>
            </div>
            <div className="my-5">
              <p className="mx-3 my-5 text-lg font-bold text-black">
                Tìm kiếm theo tên
              </p>
              <form className="flex items-center px-3">
                <label htmlFor="simple-search" className="sr-only">
                  Search
                </label>
                <div className="relative w-full">
                  <div className="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
                    <svg
                      aria-hidden="true"
                      className="h-5 w-5"
                      fill="currentColor"
                      viewBox="0 0 20 20"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        fillRule="evenodd"
                        d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                        clipRule="evenodd"
                      />
                    </svg>
                  </div>
                  <input
                    type="text"
                    id="simple-search"
                    className="block w-full rounded-lg border border-black bg-gray-50 p-2.5 pl-10 text-sm text-gray-900 focus:border-blue-500 focus:ring-blue-500  dark:border-gray-600 dark:bg-gray-700 dark:text-white dark:placeholder-gray-400 dark:focus:border-blue-500 dark:focus:ring-blue-500"
                    placeholder="Search"
                    required
                  />
                </div>
                <button
                  type="submit"
                  className="ml-2 rounded-lg border border-blue-700 bg-blue-700 p-2.5 text-sm font-medium text-white hover:bg-blue-800 focus:outline-none focus:ring-4 focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                >
                  <svg
                    className="h-5 w-5"
                    fill="none"
                    stroke="currentColor"
                    viewBox="0 0 24 24"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={2}
                      d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
                    />
                  </svg>
                  <span className="sr-only">Search</span>
                </button>
              </form>
              <div className="my-5">
                <p className="mx-3 my-5 text-lg font-bold text-black">
                  Lọc theo địa điểm
                </p>
                <form className="flex items-center px-3">
                  <label htmlFor="underline_select" className="sr-only">
                    Địa điểm
                  </label>
                  <select
                    id="underline_select"
                    className="peer block w-full appearance-none rounded-lg border-2 border-solid border-slate-800 p-2 text-sm text-black focus:border-gray-200 focus:outline-none focus:ring-0 dark:border-gray-700 dark:text-gray-400"
                  >
                    <option selected>Chọn một địa điểm</option>
                    <option>Huế</option>
                    <option>Đà Nẵng</option>
                    <option>Hạ Long</option>
                    <option>Phú Quốc</option>
                  </select>
                </form>
                <div className="my-5">
                  <p className="mx-3 my-5 text-lg font-bold text-black">
                    Lọc theo giá
                  </p>
                  <form className="flex items-center px-3">
                    <input
                      id="steps-range"
                      type="range"
                      min={0}
                      max={5}
                      defaultValue="2.5"
                      step="0.5"
                      className="h-2 w-full cursor-pointer appearance-none rounded-lg bg-gray-200 dark:bg-gray-700"
                    />
                  </form>
                </div>
                <div className="my-5">
                  <p className="mx-3 my-5 text-lg font-bold text-black">
                    Lọc theo đêm
                  </p>
                  <div className="mx-3 mb-4 flex items-center">
                    <input
                      id="default-checkbox"
                      type="checkbox"
                      className="h-4 w-4 rounded border-gray-300 bg-gray-100 text-blue-600 focus:ring-2 focus:ring-blue-500 dark:border-gray-600 dark:bg-gray-700 dark:ring-offset-gray-800 dark:focus:ring-blue-600"
                    />
                    <label
                      htmlFor="default-checkbox"
                      className="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300"
                    >
                      2 Ngày 1 Đêm
                    </label>
                  </div>
                  <div className="mx-3 mb-4 flex items-center">
                    <input
                      id="default-checkbox"
                      type="checkbox"
                      className="h-4 w-4 rounded border-gray-300 bg-gray-100 text-blue-600 focus:ring-2 focus:ring-blue-500 dark:border-gray-600 dark:bg-gray-700 dark:ring-offset-gray-800 dark:focus:ring-blue-600"
                    />
                    <label
                      htmlFor="default-checkbox"
                      className="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300"
                    >
                      3 Ngày 2 Đêm
                    </label>
                  </div>
                  <div className="mx-3 mb-4 flex items-center">
                    <input
                      id="default-checkbox"
                      type="checkbox"
                      className="h-4 w-4 rounded border-gray-300 bg-gray-100 text-blue-600 focus:ring-2 focus:ring-blue-500 dark:border-gray-600 dark:bg-gray-700 dark:ring-offset-gray-800 dark:focus:ring-blue-600"
                    />
                    <label
                      htmlFor="default-checkbox"
                      className="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300"
                    >
                      2 Ngày 2 Đêm
                    </label>
                  </div>
                  <div className="mx-3 mb-4 flex items-center">
                    <input
                      id="default-checkbox"
                      type="checkbox"
                      className="h-4 w-4 rounded border-gray-300 bg-gray-100 text-blue-600 focus:ring-2 focus:ring-blue-500 dark:border-gray-600 dark:bg-gray-700 dark:ring-offset-gray-800 dark:focus:ring-blue-600"
                    />
                    <label
                      htmlFor="default-checkbox"
                      className="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300"
                    >
                      4 Ngày 3 Đêm
                    </label>
                  </div>
                  <div className="mx-3 mb-4 flex items-center">
                    <input
                      id="default-checkbox"
                      type="checkbox"
                      className="h-4 w-4 rounded border-gray-300 bg-gray-100 text-blue-600 focus:ring-2 focus:ring-blue-500 dark:border-gray-600 dark:bg-gray-700 dark:ring-offset-gray-800 dark:focus:ring-blue-600"
                    />
                    <label
                      htmlFor="default-checkbox"
                      className="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300"
                    >
                      3 Ngày 3 Đêm
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-span-2 mt-5">
            <div className="text-center text-4xl font-bold ">
              <p>Danh Sách Tour</p>
            </div>
            <div className="mt-4 grid w-full justify-items-center">
              {tours.map((tour) => (
                <div
                  onClick={() => {
                    setOpen(true);
                    setCurrentTour(tour);
                  }}
                  key={tour._id}
                  className="my-5 flex w-full flex-col items-center rounded-lg border border-gray-500 bg-white shadow hover:bg-gray-300 dark:border-gray-700 dark:bg-gray-800 dark:hover:bg-gray-700 md:max-w-xl md:flex-row"
                >
                  <img
                    className="h-96 w-full rounded-t-lg object-cover p-1 md:h-auto md:w-48 md:rounded-none md:rounded-l-lg"
                    src={tour.image}
                    alt=""
                  />
                  <div className="flex flex-col justify-between p-4 leading-normal">
                    <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                      {tour.title}
                    </h5>
                    <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">
                      {tour.price}
                    </p>
                    <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">
                      {tour.timeDate}
                    </p>
                  </div>
                </div>
              ))}
            </div>
          </div>
          <Modal open={open} setOpen={setOpen}>
            {currentTour && <ModalDetailTour infoTour={currentTour} />}
          </Modal>
        </div>
      </div>
    </div>
  );
}
