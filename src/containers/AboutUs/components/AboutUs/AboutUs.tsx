/* eslint-disable tailwindcss/no-custom-classname */
import { Banner } from '@/components/Layout/components';
import { InstaFollow } from '@/containers/Home/components';

export default function AboutUs() {
  return (
    <div className="">
      <Banner />
      <div className=" mx-auto w-[960px]">
        <h1 className="my-5 text-center text-2xl font-bold text-red-600">
          THÔNG TIN VỀ TRAVELING
        </h1>
        <h1 className="">
          Traveling là một công ty chuyên về dịch vụ du lịch, cung cấp các gói
          tour du lịch chất lượng cao và đa dạng trong và ngoài nước. Chúng tôi
          luôn đặt mục tiêu là mang đến cho khách hàng những trải nghiệm du lịch
          tuyệt vời nhất, kết hợp giữa sự khám phá và trải nghiệm văn hóa độc
          đáo của các địa điểm du lịch hàng đầu trên thế giới. Chúng tôi cung
          cấp các tour du lịch đa dạng, từ các tour du lịch trong nước như Hà
          Nội - Hạ Long - Ninh Bình, Sài Gòn - Đà Lạt - Nha Trang, đến các tour
          du lịch ngoài nước như Nhật Bản, Hàn Quốc, Singapore, Thái Lan, Châu
          Âu, Mỹ và Canada. Bên cạnh đó, chúng tôi còn cung cấp các dịch vụ đặt
          vé máy bay, đặt khách sạn, thuê xe và hỗ trợ visa, giúp khách hàng có
          một chuyến đi thuận lợi và tiết kiệm thời gian. Đội ngũ nhân viên của
          Traveling được đào tạo chuyên nghiệp và nhiều kinh nghiệm trong lĩnh
          vực du lịch. Chúng tôi cam kết mang đến cho khách hàng những trải
          nghiệm du lịch tốt nhất, bao gồm việc chọn lựa các địa điểm du lịch
          đẹp và độc đáo, đưa ra những lời khuyên hữu ích cho khách hàng, giải
          đáp các thắc mắc của khách hàng, và hỗ trợ khách hàng trong suốt quá
          trình du lịch. Khách hàng của Traveling đánh giá cao dịch vụ của chúng
          tôi, vì chúng tôi luôn tập trung vào chất lượng và sự hài lòng của
          khách hàng. Bằng chứng là các đánh giá tích cực từ khách hàng đã sử
          dụng dịch vụ của chúng tôi. Nếu bạn có nhu cầu đặt tour du lịch hay
          muốn biết thêm thông tin chi tiết về các dịch vụ của chúng tôi, hãy
          liên hệ với chúng tôi qua địa chỉ email: info@traveling.com hoặc số
          điện thoại 0989.898.989
        </h1>
        <div className="border-black">
          <br />
          <hr />
        </div>
        <div className="text-center text-red-500">
          <p>Địa chỉ: Sô 1 - Phố Xốm - Phú Lãm - Hà Đông - Hà Nội</p>
          <p>Số điện thoại: 0989.898.989</p>
          <p>Email: info@traveling.com</p>
        </div>
      </div>
      <InstaFollow />
    </div>
  );
}
