import Layout from '@/components/Layout';

import { AboutUs } from './components';

export default function AboutUsPage() {
  return (
    <Layout>
      <AboutUs />
    </Layout>
  );
}
