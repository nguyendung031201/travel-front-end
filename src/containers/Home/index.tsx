import Layout from '@/components/Layout';
import type { IItemBanner } from '@/services/banner';
import type { IIteamTour } from '@/services/tour';

import {
  BannerSlide,
  Category,
  Destination,
  InstaFollow,
  QASuggestions,
  Tour,
} from './components';

export default function HomePage({
  banners,
  tours,
}: {
  banners: IItemBanner[];
  tours: IIteamTour[];
}) {
  return (
    <Layout>
      <BannerSlide banners={banners} />
      <Category />
      <Tour tours={tours} />
      <Destination />
      <QASuggestions />
      <InstaFollow />
    </Layout>
  );
}
