/* eslint-disable tailwindcss/no-custom-classname */
// Import Swiper styles
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import 'swiper/css/scrollbar';

import { A11y, Navigation, Pagination, Scrollbar } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';

export default function Tour() {
  return (
    <div className="">
      <div className="m-with  my-6">
        <div className="">
          <div className="flex justify-center text-3xl text-lime-500">
            THEO DÕI CHÚNG TÔI TRÊN INSTAGRAM
          </div>
          <div className="flex justify-center">
            <hr className="m-with container w-96 border-solid border-black" />
          </div>
        </div>
      </div>
      <Swiper
        className=""
        // install Swiper modules
        modules={[Navigation, Pagination, Scrollbar, A11y]}
        spaceBetween={0}
        slidesPerView={4}
        // navigation
        // pagination={{clickable: true}}
        // scrollbar={{draggable: true}}
        onSwiper={(swiper) => swiper}
        onSlideChange={() => {}}
      >
        <SwiperSlide>
          <div className="">
            <img
              src="https://oxalisadventure.com/uploads/2020/07/baida_1500x1000__637294032034947887.jpg"
              alt=""
            />
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="">
            <img
              src="https://oxalisadventure.com/uploads/2020/05/sd_1500x1000__637241697402092648.jpg"
              alt=""
            />
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="">
            <img
              src="https://oxalisadventure.com/uploads/2020/12/hangtien1_03_696x464__637424207338648083.jpg"
              alt=""
            />
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="">
            <img
              src="https://oxalisadventure.com/uploads/2021/12/tl1-696x464%20(12)__637740499994967442.jpg"
              alt=""
            />
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="">
            <img
              src="https://oxalisadventure.com/uploads/2021/08/hb_1_696x464__637645256454366047.jpg"
              alt=""
            />
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="">
            <img
              src="https://oxalisadventure.com/uploads/2020/07/baida_1500x1000__637294032034947887.jpg"
              alt=""
            />
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="">
            <img
              src="https://oxalisadventure.com/uploads/2020/05/sd_1500x1000__637241697402092648.jpg"
              alt=""
            />
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="">
            <img
              src="https://oxalisadventure.com/uploads/2020/12/hangtien1_03_696x464__637424207338648083.jpg"
              alt=""
            />
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="">
            <img
              src="https://oxalisadventure.com/uploads/2021/12/tl1-696x464%20(12)__637740499994967442.jpg"
              alt=""
            />
          </div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="">
            <img
              src="https://oxalisadventure.com/uploads/2021/08/hb_1_696x464__637645256454366047.jpg"
              alt=""
            />
          </div>
        </SwiperSlide>
        {/* <SwiperSlide>Slide 4</SwiperSlide> */}
      </Swiper>
    </div>
  );
}
