/* eslint-disable unused-imports/no-unused-vars */
/* eslint-disable react/no-unescaped-entities */
// /* eslint-disable tailwindcss/enforces-negative-arbitrary-values */
// /* eslint-disable import/no-extraneous-dependencies */

/* eslint-disable no-underscore-dangle */
/* eslint-disable tailwindcss/no-custom-classname */
// import Link from 'next/link';
import type { Settings } from 'react-slick';
import Slider from 'react-slick';
import styled from 'styled-components';

import { api } from '@/services/axios';
import type { IItemBanner } from '@/services/banner';

// export interface IItemBanner {
//   _id: string;
//   image: string;
//   number: number;
// }
// const banne: IItemBanner[] = [
//   {
//     _id: '1',
//     image:
//       'https://demo.egenslab.com/html/astrip/preview/assets/images/bg/deal11.png',
//     number: 1,
//   },
//   {
//     _id: '2',
//     image:
//       'https://demo.egenslab.com/html/astrip/preview/assets/images/bg/deal12.png',
//     number: 2,
//   },
//   {
//     _id: '3',
//     image:
//       'https://demo.egenslab.com/html/astrip/preview/assets/images/bg/deal11.png',
//     number: 3,
//   },
// ];
const Styles = styled.div`
  .title {
    text-shadow: 0px 1px 20px black;
    /* -webkit-text-stroke-width: 1px; */
    /* -webkit-text-stroke-color: #000000ca; */
  }
  .slick-dots {
    text-align: left;
    @media (min-width: 1200px) {
      text-align: center;
    }
    bottom: 1rem;
    li.slick-active {
      background: white;
      width: 50px;
      border-radius: 1rem;
      height: 10px;
    }
    li.slick-active button:before {
      color: white;
      /* width: 100px; */
    }
    li {
      background: white;
      height: 10px;
      width: 10px;
      border-radius: 1rem;
      button:before {
        color: white;
        display: none;
      }
    }
  }
  * {
    :focus-visible {
      outline: none;
    }
  }
  .btnBuy {
    .wrap {
      height: 100%;
      display: flex;
      align-items: center;
      justify-content: center;
    }

    .button {
      min-width: 300px;
      min-height: 60px;
      font-family: 'Nunito', sans-serif;
      font-size: 22px;
      text-transform: uppercase;
      letter-spacing: 1.3px;
      font-weight: 700;
      color: #313133;
      background: #4fd1c5;
      background: linear-gradient(90deg, white 0%, rgba(79, 209, 197, 1) 100%);
      border: none;
      border-radius: 1000px;
      box-shadow: 12px 12px 24px rgba(79, 209, 197, 0.64);
      transition: all 0.3s ease-in-out 0s;
      cursor: pointer;
      outline: none;
      position: relative;
      padding: 10px;
    }

    button::before {
      content: '';
      border-radius: 1000px;
      min-width: calc(300px + 12px);
      min-height: calc(60px + 12px);
      border: 6px solid #00ffcb;
      box-shadow: 0 0 60px rgba(0, 255, 203, 0.64);
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      opacity: 0;
      transition: all 0.3s ease-in-out 0s;
    }

    .button:hover,
    .button:focus {
      color: #313133;
      transform: translateY(-6px);
    }

    button:hover::before,
    button:focus::before {
      opacity: 1;
    }

    button::after {
      content: '';
      width: 30px;
      height: 30px;
      border-radius: 100%;
      border: 6px solid #00ffcb;
      position: absolute;
      z-index: -1;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      animation: ring 1.5s infinite;
    }

    button:hover::after,
    button:focus::after {
      animation: none;
      display: none;
    }

    @keyframes ring {
      0% {
        width: 30px;
        height: 30px;
        opacity: 1;
      }
      100% {
        width: 300px;
        height: 300px;
        opacity: 0;
      }
    }
  }
`;
export default function BannerSlide({ banners }: { banners: IItemBanner[] }) {
  const settings: Settings = {
    dots: true,
    infinite: true,
    speed: 330,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    arrows: false,
    nextArrow: <span />,
    prevArrow: <span />,
  };

  return (
    <>
      <Styles className="relative max-h-fit lg:min-h-[45.25rem]">
        <Slider {...settings}>
          {banners.map((bannerSlide) => (
            <div key={bannerSlide._id} className="bg-slate-600 text-center">
              <div className="h-[850px] w-full">
                <div
                  className="block min-h-[19.75rem] w-full bg-cover lg:hidden"
                  style={{ backgroundImage: `url(${api}${bannerSlide.image})` }}
                />

                <div className="hidden h-full w-full lg:block">
                  <img
                    src={bannerSlide.image}
                    alt=""
                    className="w-full lg:min-h-[45rem]"
                  />
                </div>
              </div>
            </div>
          ))}
        </Slider>
        <div className="pointer-events-none absolute top-0 z-10 flex h-full w-full flex-col items-center justify-center px-6 text-center lg:h-fit lg:px-9">
          <h1 className="title pointer-events-none mb-4 font-[Cormorant] text-2xl font-semibold text-white lg:mb-10 lg:mt-16 lg:max-w-[59.75rem] lg:text-5xl">
            Traveling luôn đem đến cho bạn những Tour du lịch và trải nghiệm tốt
            nhất
          </h1>
        </div>
      </Styles>
    </>
  );
}
