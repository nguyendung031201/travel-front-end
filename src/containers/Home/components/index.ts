import dynamic from 'next/dynamic';

export { default as Tour } from './Tour/Tour';
const BannerSlide = dynamic(() => import('./BannerSlide/BannerSlide'), {
  ssr: false,
});

export { BannerSlide };
export { default as Category } from './Category/Category';
export { default as Destination } from './Destination/Destination';
export { default as InstaFollow } from './InstaFollow/InstaFollow';
export { default as QASuggestions } from './QASuggestions/QASuggestions';
