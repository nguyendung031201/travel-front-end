/* eslint-disable tailwindcss/migration-from-tailwind-2 */

import router from 'next/router';
import { useState } from 'react';
import { toast } from 'react-toastify';

import type { IItemFeedback } from '@/services/feedback';
import servicesFeedback from '@/services/feedback';

/* eslint-disable tailwindcss/no-custom-classname */
interface IFeedback {
  dataFeedback?: IItemFeedback;
  reload: () => Promise<void>;
}
export default function QASuggestions({ dataFeedback, reload }: IFeedback) {
  const [firstName, setFirstName] = useState<string>(
    dataFeedback?.firstName || ''
  );
  const [lastName, setLastName] = useState<string>(
    dataFeedback?.lastName || ''
  );
  const [email, setEmail] = useState<string>(dataFeedback?.email || '');
  const [feedBack, setFeedBack] = useState<string>(
    dataFeedback?.feedBack || ''
  );

  const handleSave = async () => {
    const dataFeedBackNew: IItemFeedback = {
      _id: '',
      firstName,
      lastName,
      email,
      feedBack,
    };
    const resSave = await servicesFeedback.create(dataFeedBackNew);
    if (resSave.data.success) {
      toast.success('Update thành công');
      reload();
    } else {
      toast.error('Lỗi');
    }
    router.push('/');
  };

  return (
    <div className="container mx-auto my-5">
      <div className="m-with">
        <div className="">
          <div className="flex justify-center text-3xl text-lime-500">
            HỎI ĐÁP VÀ GÓP Ý
          </div>
          <div className="flex justify-center">
            <hr className="m-with container w-96 border-solid border-black" />
          </div>
        </div>
      </div>
      <div className="flex justify-center">
        <div className="container my-5 max-w-2xl ">
          <div className="mt-5">
            <label className="mb-2 block text-xl font-bold text-gray-900 dark:text-white ">
              First name
            </label>
            <input
              value={firstName}
              onChange={(e) => {
                setFirstName(e.target.value);
              }}
              type="text"
              className="block w-full rounded-lg border border-gray-300 bg-gray-50 p-2.5 text-sm text-gray-900 focus:border-blue-500 focus:ring-blue-500 dark:border-gray-600 dark:bg-gray-700 dark:text-white dark:placeholder:text-gray-400 dark:focus:border-blue-500 dark:focus:ring-blue-500"
              required
            />
          </div>
          <div className="mt-5">
            <label
              htmlFor="text"
              className="mb-2 block text-xl font-bold text-gray-900 dark:text-white"
            >
              Last name
            </label>
            <input
              value={lastName}
              onChange={(e) => {
                setLastName(e.target.value);
              }}
              type="text"
              className="block w-full rounded-lg border border-gray-300 bg-gray-50 p-2.5 text-sm text-gray-900 focus:border-blue-500 focus:ring-blue-500 dark:border-gray-600 dark:bg-gray-700 dark:text-white dark:placeholder:text-gray-400 dark:focus:border-blue-500 dark:focus:ring-blue-500"
              required
            />
          </div>
          <div className="mt-5">
            <label
              htmlFor="email"
              className="mb-2 block text-xl font-bold text-gray-900 dark:text-white"
            >
              Your email
            </label>
            <input
              value={email}
              onChange={(e) => {
                setEmail(e.target.value);
              }}
              type="email"
              placeholder=" email@gmail.com"
              className="block w-full rounded-lg border border-gray-300 bg-gray-50 p-2.5 text-sm text-gray-900 focus:border-blue-500 focus:ring-blue-500 dark:border-gray-600 dark:bg-gray-700 dark:text-white dark:placeholder:text-gray-400 dark:focus:border-blue-500 dark:focus:ring-blue-500"
              required
            />
          </div>
          <div className="mt-5">
            <label
              htmlFor="message"
              className="mb-2 block text-xl font-bold text-gray-900 dark:text-white"
            >
              Your message
            </label>
            <textarea
              value={feedBack}
              onChange={(e) => {
                setFeedBack(e.target.value);
              }}
              rows={7}
              className="block w-full rounded-lg border border-gray-300 bg-gray-50 p-2.5 text-sm text-gray-900 focus:border-blue-500 focus:ring-blue-500 dark:border-gray-600 dark:bg-gray-700 dark:text-white dark:placeholder:text-gray-400 dark:focus:border-blue-500 dark:focus:ring-blue-500"
              placeholder="Write your thoughts here..."
              defaultValue={''}
            />
          </div>
        </div>
      </div>
      <div className="flex justify-center px-4 py-3  text-right sm:px-6 ">
        <button
          onClick={handleSave}
          className="group relative mb-2 mr-2 inline-flex items-center justify-center overflow-hidden rounded-lg bg-gradient-to-br from-teal-300 to-lime-300 p-0.5 text-sm font-medium text-gray-900 focus:outline-none focus:ring-4 focus:ring-lime-200 group-hover:from-teal-300 group-hover:to-lime-300 dark:text-white dark:hover:text-gray-900 dark:focus:ring-lime-800"
        >
          <span className="relative rounded-md bg-white px-5 py-2.5 transition-all duration-75 ease-in group-hover:bg-opacity-0 dark:bg-gray-900">
            Submit
          </span>
        </button>
      </div>
    </div>
  );
}
