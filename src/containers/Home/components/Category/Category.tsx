/* eslint-disable tailwindcss/no-custom-classname */
export default function Category() {
  return (
    <div className="container mx-auto my-5">
      <div className="container">
        <div className="m-with">
          <div className="">
            <div className="flex justify-center text-3xl text-lime-500">
              DANH MỤC DU LỊCH
            </div>
            <div className="flex justify-center text-5xl">
              <p>Loại tour phổ biến của chúng tôi</p>
            </div>
            <div className="flex justify-center">
              <hr className="m-with container w-96 border-solid border-black" />
            </div>
          </div>
        </div>
        <div className="container">
          <div className="grid grid-cols-2 gap-4 p-10 md:grid-cols-6 ">
            <div className="grid justify-center rounded-full border-2 border-solid border-inherit bg-gray-200 p-16 transition duration-300 ease-in-out hover:scale-110">
              <img
                className="h-auto max-w-full rounded-lg"
                src="https://astrip-wp.b-cdn.net/wp-content/uploads/2023/01/adventure.svg"
                alt=""
              />
              <p>Adventure</p>
            </div>
            <div className="grid justify-center rounded-full border-2 border-solid border-inherit bg-gray-200 p-16 transition duration-300 ease-in-out hover:scale-110">
              <img
                className="h-auto max-w-full rounded-lg"
                src="https://astrip-wp.b-cdn.net/wp-content/uploads/2023/01/city-tour.svg"
                alt=""
              />
              <p>City Tour</p>
            </div>
            <div className="grid justify-center rounded-full border-2 border-solid border-inherit bg-gray-200 p-16 transition duration-300 ease-in-out hover:scale-110">
              <img
                className="h-auto max-w-full rounded-lg"
                src="https://astrip-wp.b-cdn.net/wp-content/uploads/2023/01/sheep.svg"
                alt=""
              />
              <p>Cruises Tour</p>
            </div>
            <div className="grid justify-center rounded-full border-2 border-solid border-inherit bg-gray-200 p-16 transition duration-300 ease-in-out hover:scale-110">
              <img
                className="h-auto max-w-full rounded-lg"
                src="https://astrip-wp.b-cdn.net/wp-content/uploads/2023/01/sea-tour.svg"
                alt=""
              />
              <p>Sea Tour</p>
            </div>
            <div className="grid justify-center rounded-full border-2 border-solid border-inherit bg-gray-200 p-16 transition duration-300 ease-in-out hover:scale-110">
              <img
                className="h-auto max-w-full rounded-lg"
                src="https://astrip-wp.b-cdn.net/wp-content/uploads/2023/01/sea-tour.svg"
                alt=""
              />
              <p>Travel</p>
            </div>
            <div className="grid justify-center rounded-full border-2 border-solid border-inherit bg-gray-200 p-16 transition duration-300 ease-in-out hover:scale-110">
              <img
                className="h-auto max-w-full rounded-lg"
                src="https://astrip-wp.b-cdn.net/wp-content/uploads/2023/01/wedding.svg"
                alt=""
              />
              <p>Wedding</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
