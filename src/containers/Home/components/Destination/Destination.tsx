import Link from 'next/link';

/* eslint-disable tailwindcss/no-custom-classname */
export default function Destination() {
  return (
    <div className="cotainer mx-auto my-5">
      <div className="m-with">
        <div className="">
          <div className="flex justify-center text-3xl text-lime-500">
            DANH MỤC DU LỊCH
          </div>
          <div className="flex justify-center text-5xl">
            <p>Loại tour phổ biến của chúng tôi</p>
          </div>
          <div className="flex justify-center">
            <hr className="m-with container w-96 border-solid border-black" />
          </div>
        </div>
      </div>
      <div className="m-5 grid grid-cols-2 gap-4 md:grid-cols-4">
        <figure className="relative max-w-xl transition duration-300 ease-in-out hover:scale-110 ">
          <Link href="#">
            <img
              className="rounded-lg"
              src="https://hanoitop10.net/wp-content/uploads/anh-ha-noi-dep-ve-dem_013050944.jpg"
              alt="image description"
            />
          </Link>
          <figcaption className="absolute bottom-6  px-4">
            <p className="item-center flex text-2xl font-bold text-white ">
              Hà Nội
            </p>
          </figcaption>
        </figure>
        <figure className="relative max-w-xl transition duration-300 ease-in-out hover:scale-110 ">
          <Link href="#">
            <img
              className="rounded-lg"
              src="https://hanoitop10.net/wp-content/uploads/anh-ha-noi-dep-ve-dem_013050944.jpg"
              alt="image description"
            />
          </Link>
          <figcaption className="absolute bottom-6  px-4">
            <p className="item-center flex text-2xl font-bold text-white">
              Hà Nội
            </p>
          </figcaption>
        </figure>
        <figure className="relative max-w-xl  transition duration-300 ease-in-out hover:scale-110">
          <Link href="#">
            <img
              className="rounded-lg"
              src="https://hanoitop10.net/wp-content/uploads/anh-ha-noi-dep-ve-dem_013050944.jpg"
              alt="image description"
            />
          </Link>
          <figcaption className="absolute bottom-6  px-4">
            <p className="item-center flex text-2xl font-bold text-white">
              Hà Nội
            </p>
          </figcaption>
        </figure>
        <figure className="relative max-w-xl transition duration-300 ease-in-out hover:scale-110 ">
          <Link href="#">
            <img
              className="rounded-lg"
              src="https://hanoitop10.net/wp-content/uploads/anh-ha-noi-dep-ve-dem_013050944.jpg"
              alt="image description"
            />
          </Link>
          <figcaption className="absolute bottom-6  px-4">
            <p className="item-center flex text-2xl font-bold text-white">
              Hà Nội
            </p>
          </figcaption>
        </figure>
      </div>
    </div>
  );
}
