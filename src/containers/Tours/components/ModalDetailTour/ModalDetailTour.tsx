/* eslint-disable tailwindcss/migration-from-tailwind-2 */
import router from 'next/router';
import { useState } from 'react';
import { toast } from 'react-toastify';

import type { IItemBooking } from '@/services/booking';
import servicesBooking from '@/services/booking';
import type { IIteamTour } from '@/services/tour';

export default function ModalDetailTour({
  reload,
  infoTour,
  bookingTour,
}: {
  infoTour: IIteamTour;
  bookingTour: IItemBooking;
  reload: () => Promise<void>;
}) {
  const [email, setEmail] = useState<string>(String(bookingTour?.email || ''));
  const [fullName, setFullName] = useState<string>(bookingTour?.fullName || '');
  const [people, setPeople] = useState<string>(bookingTour?.people || '');
  const [phoneNumber, setPhoneNumber] = useState<string>(
    bookingTour?.phoneNumber || ''
  );
  const [note, setNote] = useState<string>(bookingTour?.note || '');

  const handleSave = async () => {
    const bookingTourNew: IItemBooking = {
      _id: '',
      email,
      fullName,
      people,
      phoneNumber,
      note,
      startDate: '',
      tour: '',
      status: '',
    };
    const resSave = await servicesBooking.create(bookingTourNew);
    if (resSave.data.success) {
      reload();
      toast.success('Đặt thành công');
    } else {
      toast.error('LLỗiLỗi');
    }
    router.push('/');
  };
  return (
    <>
      <div className="">
        {/* <div className="">
          <Banner />
        </div> */}
        <div className="grid grid-cols-3 gap-4">
          <div className="col-span-2">
            <div className="">
              <p className="m-5 text-center text-4xl font-bold">
                THÔNG TIN TOUR DU LỊCH
              </p>
            </div>
            <div className="text-center text-xl">
              <p>TOUR {infoTour.title}</p>
              <p>Số người: {infoTour.limitPeople}</p>
              <p className="text-red-700">Giá: {infoTour.price} VNĐ</p>
              <p>Thời gian: {infoTour.timeDate}</p>
              <p className="!text-left font-bold">Thông tin dịch vụ:</p>
              <p className="!text-left">{infoTour.tourDetail}</p>
              <img src={infoTour.image} alt="" />
            </div>
          </div>
          <div className="m-5">
            <div className="">
              <p className="mb-3 bg-slate-200 text-center text-2xl font-bold text-red-600">
                ĐẶT TOUR
              </p>
            </div>
            <div className="">
              <form>
                <div className="group relative z-0 mb-6 w-full">
                  <input
                    type="email"
                    name="floating_email"
                    id="floating_email"
                    className="peer block w-full appearance-none border-0 border-b-2 border-gray-300 bg-transparent px-0 py-2.5 text-sm text-gray-900 focus:border-blue-600 focus:outline-none focus:ring-0 dark:border-gray-600 dark:text-white dark:focus:border-blue-500"
                    placeholder=" "
                    required
                    value={email}
                    onChange={(e) => {
                      setEmail(e.target.value);
                    }}
                  />
                  <label
                    htmlFor="floating_email"
                    className="absolute top-3 -z-10 origin-[0] -translate-y-6 scale-75 text-sm text-gray-500 duration-300 peer-placeholder-shown:translate-y-0 peer-placeholder-shown:scale-100 peer-focus:left-0 peer-focus:-translate-y-6 peer-focus:scale-75 peer-focus:font-medium peer-focus:text-blue-600 dark:text-gray-400 peer-focus:dark:text-blue-500"
                  >
                    Email
                  </label>
                </div>
                <div className="group relative z-0 mb-6 w-full">
                  <input
                    className="peer block w-full appearance-none border-0 border-b-2 border-gray-300 bg-transparent px-0 py-2.5 text-sm text-gray-900 focus:border-blue-600 focus:outline-none focus:ring-0 dark:border-gray-600 dark:text-white dark:focus:border-blue-500"
                    placeholder=" "
                    required
                    value={fullName}
                    onChange={(e) => {
                      setFullName(e.target.value);
                    }}
                  />
                  <label className="absolute top-3 -z-10 origin-[0] -translate-y-6 scale-75 text-sm text-gray-500 duration-300 peer-placeholder-shown:translate-y-0 peer-placeholder-shown:scale-100 peer-focus:left-0 peer-focus:-translate-y-6 peer-focus:scale-75 peer-focus:font-medium peer-focus:text-blue-600 dark:text-gray-400 peer-focus:dark:text-blue-500">
                    Tên đầy đủ
                  </label>
                </div>
                {/* <div className="group relative z-0 mb-6 w-full">
                  <input
                    type="password"
                    name="floating_password"
                    className="peer block w-full appearance-none border-0 border-b-2 border-gray-300 bg-transparent px-0 py-2.5 text-sm text-gray-900 focus:border-blue-600 focus:outline-none focus:ring-0 dark:border-gray-600 dark:text-white dark:focus:border-blue-500"
                    placeholder=" "
                    required
                  />
                  <label
                    htmlFor="floating_password"
                    className="absolute top-3 -z-10 origin-[0] -translate-y-6 scale-75 text-sm text-gray-500 duration-300 peer-placeholder-shown:translate-y-0 peer-placeholder-shown:scale-100 peer-focus:left-0 peer-focus:-translate-y-6 peer-focus:scale-75 peer-focus:font-medium peer-focus:text-blue-600 dark:text-gray-400 peer-focus:dark:text-blue-500"
                  >
                    Chọn ngày bắt đầu khởi hành
                  </label>
                </div> */}
                <div className="">
                  <div className="relative z-0 mb-6 w-full">
                    <input
                      type="text"
                      name="floating_first_name"
                      id="floating_first_name"
                      className="peer block w-full appearance-none border-0 border-b-2 border-gray-300 bg-transparent px-0 py-2.5 text-sm text-gray-900 focus:border-blue-600 focus:outline-none focus:ring-0 dark:border-gray-600 dark:text-white dark:focus:border-blue-500"
                      placeholder=" "
                      required
                      value={people}
                      onChange={(e) => {
                        setPeople(e.target.value);
                      }}
                    />
                    <label
                      htmlFor="floating_first_name"
                      className="absolute top-3 -z-10 origin-[0] -translate-y-6 scale-75 text-sm text-gray-500 duration-300 peer-placeholder-shown:translate-y-0 peer-placeholder-shown:scale-100 peer-focus:left-0 peer-focus:-translate-y-6 peer-focus:scale-75 peer-focus:font-medium peer-focus:text-blue-600 dark:text-gray-400 peer-focus:dark:text-blue-500"
                    >
                      Số lượng người
                    </label>
                  </div>
                </div>
                <div className="">
                  <div className="relative z-0 mb-6 w-full">
                    <input
                      type="tel"
                      name="floating_phone"
                      className="peer block w-full appearance-none border-0 border-b-2 border-gray-300 bg-transparent px-0 py-2.5 text-sm text-gray-900 focus:border-blue-600 focus:outline-none focus:ring-0 dark:border-gray-600 dark:text-white dark:focus:border-blue-500"
                      placeholder=" "
                      required
                      value={phoneNumber}
                      onChange={(e) => {
                        setPhoneNumber(e.target.value);
                      }}
                    />
                    <label
                      htmlFor="floating_phone"
                      className="absolute top-3 -z-10 origin-[0] -translate-y-6 scale-75 text-sm text-gray-500 duration-300 peer-placeholder-shown:translate-y-0 peer-placeholder-shown:scale-100 peer-focus:left-0 peer-focus:-translate-y-6 peer-focus:scale-75 peer-focus:font-medium peer-focus:text-blue-600 dark:text-gray-400 peer-focus:dark:text-blue-500"
                    >
                      Số điện thoại
                    </label>
                  </div>
                  <div className="">
                    <div className="relative z-0 mb-6 w-full">
                      <label
                        htmlFor="floating_first_name"
                        className="absolute top-3 -z-10 origin-[0] -translate-y-6 scale-75 text-xl text-gray-500 duration-300 peer-placeholder-shown:translate-y-0 peer-placeholder-shown:scale-100 peer-focus:left-0 peer-focus:-translate-y-6 peer-focus:scale-75 peer-focus:font-medium peer-focus:text-blue-600 dark:text-gray-400 peer-focus:dark:text-blue-500"
                      >
                        Ghi chú
                      </label>
                      <textarea
                        value={note}
                        onChange={(e) => {
                          setNote(e.target.value);
                        }}
                        rows={7}
                        className=" mt-5 w-full rounded-lg border border-gray-300 bg-gray-50 p-2.5 text-xl text-gray-900 focus:border-blue-500 focus:ring-blue-500 dark:border-gray-600 dark:bg-gray-700 dark:text-white dark:placeholder:text-gray-400 dark:focus:border-blue-500 dark:focus:ring-blue-500"
                        placeholder="Write your thoughts here..."
                        defaultValue={''}
                      />
                    </div>
                  </div>
                </div>
                <button
                  onClick={handleSave}
                  type="submit"
                  className="w-full rounded-lg bg-blue-700 px-5 py-2.5 text-center text-sm font-medium text-white hover:bg-blue-800 focus:outline-none focus:ring-4 focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800 sm:w-auto"
                >
                  Đặt tour
                </button>
              </form>
            </div>
            <div />
          </div>
        </div>
      </div>
    </>
  );
}
