/* eslint-disable @typescript-eslint/no-redeclare */
/* eslint-disable no-underscore-dangle */

import { get, post, put, remove } from './axios';

export interface IItemUser {
  _id: string;
  username: string;
  password: string;
  role: string;
}

const getAll: () => Promise<{
  data: {
    user: IItemUser[];
  };
}> = async () => {
  try {
    const res = await get('/user/getAll');
    return res;
  } catch (error: any) {
    return error?.response;
  }
};

const getById = async (_id: string | number) => {
  try {
    const res = await get(`/user/${_id}`);
    return res;
  } catch (error: any) {
    return error?.response;
  }
};

const removeM: (_id: string | number) => Promise<{
  data: {
    success: boolean;
  };
}> = async (_id: string | number) => {
  try {
    const res = await remove(`/user/${_id}`);
    return res;
  } catch (error: any) {
    return error?.response;
  }
};

const create = async (data: IItemUser) => {
  const params: any = { ...data };

  delete params._id;

  try {
    const res = await post(`/user/createUser`, params);
    return res;
  } catch (error: any) {
    return error?.response;
  }
};

const update = async (data: IItemUser) => {
  const params: any = { ...data };

  delete params._id;

  try {
    const res = await put(`/user/${data._id}`, params);
    return res;
  } catch (error: any) {
    return error?.response;
  }
};

const servicesUser = { getAll, removeM, getById, create, update };

export default servicesUser;
