/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable unused-imports/no-unused-vars */
import axios from 'axios';

export const api = 'http://localhost:3001';

export const axiosCustom = axios.create({
  baseURL: api,
  timeout: 1000,
  headers: {
    'X-Custom-Header': 'foobar',
  },
});
const redirect = () => {
  if (typeof window !== 'undefined') {
    window.location.replace('/admin/login');
  }
};
export const get = async (endpoint: string, configs?: any) => {
  const res = await axiosCustom.get(endpoint, configs);
  return res;
};
export const post = async (endpoint: string, params?: any, configs?: any) => {
  const res = await axiosCustom.post(endpoint, params, configs);
  if (res.status === 401) {
    redirect();
  }
  return res;
};
export const put = async (endpoint: string, params?: any, configs?: any) => {
  const res = await axiosCustom.put(endpoint, params, configs);
  if (res.status === 401) {
    redirect();
  }
  return res;
};
export const remove = async (endpoint: string, configs?: any) => {
  const res = await axiosCustom.delete(endpoint, configs);
  if (res.status === 401) {
    redirect();
  }
  return res;
};
