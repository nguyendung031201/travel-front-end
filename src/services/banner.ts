/* eslint-disable @typescript-eslint/no-redeclare */
/* eslint-disable no-underscore-dangle */

import { get, post, put, remove } from './axios';

export interface IItemBanner {
  _id: string;
  number: number;
  image: string;
}

const getAll: () => Promise<{
  data: {
    banner: IItemBanner[];
  };
}> = async () => {
  try {
    const res = await get('/banner/getAll');
    return res;
  } catch (error: any) {
    return error?.response;
  }
};

const getById = async (_id: string | number) => {
  try {
    const res = await get(`/banner/${_id}`);
    return res;
  } catch (error: any) {
    return error?.response;
  }
};

const removeM: (_id: string | number) => Promise<{
  data: {
    success: boolean;
  };
}> = async (_id: string | number) => {
  try {
    const res = await remove(`/banner/${_id}`);
    return res;
  } catch (error: any) {
    return error?.response;
  }
};

const create = async (data: IItemBanner) => {
  const params: any = { ...data };

  delete params._id;

  try {
    const res = await post(`/banner`, params);
    return res;
  } catch (error: any) {
    return error?.response;
  }
};

const update = async (data: IItemBanner) => {
  const params: any = { ...data };

  delete params._id;

  try {
    const res = await put(`/banner/${data._id}`, params);
    return res;
  } catch (error: any) {
    return error?.response;
  }
};

const servicesBanner = { getAll, removeM, getById, create, update };

export default servicesBanner;
