import { post } from './axios';

const createBase64 = async (base64: string) => {
  try {
    const res = await post(`/upload/image64`, { base64 });
    return res;
  } catch (error: any) {
    return error?.response;
  }
};

const servicesUpload = { createBase64 };

export default servicesUpload;
