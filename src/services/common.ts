/* eslint-disable no-underscore-dangle */
import type { ICommon } from '@/containers/Admin/Common/CommonEdit';

import { get, post, put, remove } from './axios';

const getAll = async () => {
  try {
    const res = await get('/common/getAll');
    return res;
  } catch (error: any) {
    return error?.response;
  }
};

const getById = async (_id: string) => {
  try {
    const res = await get(`/common/${_id}`);
    return res;
  } catch (error: any) {
    return error?.response;
  }
};

const removeM: (_id: string) => Promise<{
  data: {
    success: boolean;
  };
}> = async (_id: string) => {
  try {
    const res = await remove(`/common/${_id}`);
    return res;
  } catch (error: any) {
    return error?.response;
  }
};

const create = async (data: ICommon) => {
  const params: any = { ...data };

  delete params._id;

  try {
    const res = await post(`/common`, params);
    return res;
  } catch (error: any) {
    return error?.response;
  }
};

const update = async (data: ICommon) => {
  const params: any = { ...data };

  delete params._id;

  try {
    const res = await put(`/common/${data._id}`, params);
    return res;
  } catch (error: any) {
    return error?.response;
  }
};

const servicesCommon = { removeM, getById, create, update, getAll };

export default servicesCommon;
