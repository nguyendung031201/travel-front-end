/* eslint-disable @typescript-eslint/no-redeclare */
/* eslint-disable no-underscore-dangle */

import { get, post, put, remove } from './axios';

export interface IItemFeedback {
  _id: string;
  firstName: string;
  lastName: string;
  email: string;
  feedBack: string;
}

const getAll: () => Promise<{
  data: {
    feedback: IItemFeedback[];
  };
}> = async () => {
  try {
    const res = await get('/feedback/getAll');
    return res;
  } catch (error: any) {
    return error?.response;
  }
};

const getById = async (_id: string | number) => {
  try {
    const res = await get(`/feedback/${_id}`);
    return res;
  } catch (error: any) {
    return error?.response;
  }
};

const removeM: (_id: string | number) => Promise<{
  data: {
    success: boolean;
  };
}> = async (_id: string | number) => {
  try {
    const res = await remove(`/feedback/${_id}`);
    return res;
  } catch (error: any) {
    return error?.response;
  }
};

const create = async (data: IItemFeedback) => {
  const params: any = { ...data };

  delete params._id;

  try {
    const res = await post(`/feedback`, params);
    return res;
  } catch (error: any) {
    return error?.response;
  }
};

const update = async (data: IItemFeedback) => {
  const params: any = { ...data };

  delete params._id;

  try {
    const res = await put(`/feedback/${data._id}`, params);
    return res;
  } catch (error: any) {
    return error?.response;
  }
};

const servicesFeedback = { getAll, removeM, getById, create, update };

export default servicesFeedback;
