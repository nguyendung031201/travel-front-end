/* eslint-disable @typescript-eslint/no-redeclare */
/* eslint-disable no-underscore-dangle */

import { get, post, put, remove } from './axios';

export interface IItemBooking {
  _id: string;
  email: String;
  fullName: string;
  startDate: string;
  people: string;
  phoneNumber: string;
  note: string;
  tour: string;
  status: string;
}

const getAll: () => Promise<{
  data: {
    booking: IItemBooking[];
  };
}> = async () => {
  try {
    const res = await get('/booking/getAll');
    return res;
  } catch (error: any) {
    return error?.response;
  }
};

const getById = async (_id: string | number) => {
  try {
    const res = await get(`/booking/${_id}`);
    return res;
  } catch (error: any) {
    return error?.response;
  }
};

const removeM: (_id: string | number) => Promise<{
  data: {
    success: boolean;
  };
}> = async (_id: string | number) => {
  try {
    const res = await remove(`/booking/${_id}`);
    return res;
  } catch (error: any) {
    return error?.response;
  }
};

const create = async (data: IItemBooking) => {
  const params: any = { ...data };

  delete params._id;

  try {
    const res = await post(`/booking/create`, params);
    return res;
  } catch (error: any) {
    return error?.response;
  }
};

const update = async (data: IItemBooking) => {
  const params: any = { ...data };

  delete params._id;

  try {
    const res = await put(`/booking/${data._id}`, params);
    return res;
  } catch (error: any) {
    return error?.response;
  }
};

const servicesBooking = { getAll, removeM, getById, create, update };

export default servicesBooking;
