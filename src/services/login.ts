/* eslint-disable unused-imports/no-unused-vars */
import { post } from './axios';

const login = async (data: any, nextFunc: () => void) => {
  try {
    const res = await post('/user/login', data);
    nextFunc();
    return res;
  } catch (error: any) {
    return error?.response;
  }
};

const logout = (nextFunc: () => void) => {
  localStorage.removeItem('user');
  nextFunc();
};

const servicesLogin = { login, logout };

export default servicesLogin;
