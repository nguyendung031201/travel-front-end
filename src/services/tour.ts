/* eslint-disable @typescript-eslint/no-redeclare */
/* eslint-disable no-underscore-dangle */

import { get, post, put, remove } from './axios';

export interface IIteamTour {
  _id: string;
  title: string;
  image: string;
  timeDate: string;
  price: string;
  content: string;
  limitPeople: string;
  location: string;
  tourDetail: string;
}

const getAll: () => Promise<{
  data: {
    tour: IIteamTour[];
  };
}> = async () => {
  try {
    const res = await get('/tour/getAll');
    return res;
  } catch (error: any) {
    return error?.response;
  }
};

const getById = async (_id: string | number) => {
  try {
    const res = await get(`/tour/${_id}`);
    return res;
  } catch (error: any) {
    return error?.response;
  }
};

const removeM: (_id: string | number) => Promise<{
  data: {
    success: boolean;
  };
}> = async (_id: string | number) => {
  try {
    const res = await remove(`/tour/${_id}`);
    return res;
  } catch (error: any) {
    return error?.response;
  }
};

const create = async (data: IIteamTour) => {
  const params: any = { ...data };

  delete params._id;

  try {
    const res = await post(`/tour`, params);
    return res;
  } catch (error: any) {
    return error?.response;
  }
};

const update = async (data: IIteamTour) => {
  const params: any = { ...data };

  delete params._id;

  try {
    const res = await put(`/tour/${data._id}`, params);
    return res;
  } catch (error: any) {
    return error?.response;
  }
};

const servicesTour = { getAll, removeM, getById, create, update };

export default servicesTour;
